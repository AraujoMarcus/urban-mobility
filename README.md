
 - 1.This project arisen from the curiosity to identify hot spots and analyze criminal behavior in urban scenario. All codes are exploratory attempts to reach results, reason why some of them are currently useless or not connected directly. 

 - 2.Our first objective is recreating cities using graph model G = (E,V). Such that edges are equivalent to streets and nodes, to corners. Our primordial source is the OpenStreetMap project. And the useful data to the present data flow are nodes = {lat, lon, id} and edges = {source_node, destiny_node, id, length}.
 
 - 3.Mapping crimes is the second step. It can be done in two different approaches:
		-3.1 knn, k=1  - calculating the distance to each node and correlating to the nearest one 
		-3.2 sparse matrix - create a sparse matrix using nodes, maintaining the spatial distribution, so the mapping occur in 3 steps: 1. use lat and lon indexes to find the closest virtual cordinate to input crime; 2. increase the indexes radius until a computed node isn`t reached; 3. link the crime to the reached node. 

 - 4. Extract criminal communities - code not available 
 
 - 5. Finally, some measures are applied :
	
	 - 5.1 Calculate homogeneity and completeness scores 

 	 - 5.2 Calculate DTW and pearson correlation between nodes containing crimes and its neighbors in i-th order 
	
 
 

