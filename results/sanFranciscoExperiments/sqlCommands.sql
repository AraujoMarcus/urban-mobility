﻿sudo yum install postgis*

-- Enable PostGIS (includes raster)
CREATE EXTENSION postgis;
-- Enable Topology
CREATE EXTENSION postgis_topology;
-- fuzzy matching needed for Tiger
CREATE EXTENSION fuzzystrmatch;

Adicione um espaço na linha acima de 1873698, onde tem vírgulas seguidas
sed "s/'//g" SFPD_Incidents_-_from_1_January_2003.csv | awk '{if(NR>1) print "INSERT INTO incident (category, descript, dayOfWeek, dayIncident, PdDistrict, resolution, address, pos) VALUES (\x27"$2"\x27, \x27"$3"\x27, \x27"$4"\x27, to_timestamp(\x27"$5" "$6"\x27, \x27MM/DD/YYYY HH24:MI\x27), \x27"$7"\x27, \x27"$8"\x27, \x27"$9"\x27, ST_GeomFromText(\x27POINT("$10" "$11")\x27, 4326));" }' FPAT='([^,]+)|("[^"]+")' | sed 's/\"//g' > incidents.sql

DROP TABLE incident;
CREATE TABLE incident ( 
	id 		SERIAL PRIMARY KEY,
	category	VARCHAR(30),
	descript	VARCHAR(100),
	dayOfWeek	VARCHAR(12),
	dayIncident	TIMESTAMP,
	PdDistrict	VARCHAR(30),
	resolution	VARCHAR(100),
	address		VARCHAR(200),
	pos 		GEOMETRY(Point, 4326)
);
CREATE INDEX incident_gix ON incident USING GIST(pos); 

-- Cidade, com os dados do Gabriel
awk -F, '{if(NR<=5){next} split($0,a," "); print "INSERT INTO vertices(id, pos) VALUES ("a[1]", ST_GeomFromText(\x27POINT("a[3] " " a[2]")\x27, 4326));" }' san-francisco_noisy-vertices.txt > san-francisco_noisy-vertices.sql

DROP TABLE vertices;
CREATE TABLE vertices ( 
	id 		BIGINT PRIMARY KEY,
	pos 		GEOMETRY(Point, 4326)
);
CREATE INDEX vertices_gix ON vertices USING GIST(pos); 

awk -F, '{if(NR<=4){next} split($0,a," "); print "INSERT INTO edges(startNode, endNode, distance) VALUES ("a[1]", "a[2]", "a[3]");" }' san-francisco_noisy-edges.txt > san-francisco_noisy-edges.sql
DROP TABLE edges;
CREATE TABLE edges ( 
	startNode	BIGINT,
	endNode		BIGINT,
	distance	DECIMAL	
);

DROP TABLE streets;
CREATE TABLE streets AS
	SELECT ST_MAKELINE(V1.pos, V2.pos) as pos, edges.distance as dist
	FROM edges JOIN vertices V1 on V1.id = edges.startNode
		JOIN vertices V2 on V2.id = edges.endNode;
CREATE INDEX streets_gix ON streets USING GIST(pos); 


-- Cidade, com os dados do Gabriel
echo 'BEGIN;' > /tmp/outSorted.sql
awk -F, '{if(NR<=1){next} split($0,a,","); print "INSERT INTO verticesIncident(id, pos, CrimesAmount,WARRANTS,OTHER_OFFENSES,LARCENY_THEFT,VEHICLE_THEFT,VANDALISM,NON_CRIMINAL,ROBBERY,ASSAULT,WEAPON_LAWS,BURGLARY,SUSPICIOUS_OCC,DRUNKENNESS,FORGERY_COUNTERFEITING,DRUG_NARCOTIC,STOLEN_PROPERTY,SECONDARY_CODES,TRESPASS,MISSING_PERSON,FRAUD,KIDNAPPING,RUNAWAY,DRIVING_UNDER_THE_INFLUENCE,SEX_OFFENSES__FORCIBLE,PROSTITUTION,DISORDERLY_CONDUCT,ARSON,FAMILY_OFFENSES,LIQUOR_LAWS,BRIBERY,EMBEZZLEMENT,SUICIDE,LOITERING,SEX_OFFENSES__NON_FORCIBLE,EXTORTION,GAMBLING,BAD_CHECKS,TREA,RECOVERED_VEHICLE,PORNOGRAPHY_OBSCENE_MAT) VALUES ("a[1]", ST_GeomFromText(\x27POINT("a[3] " " a[2]")\x27, 4326),"a[4]","a[5]","a[6]","a[7]","a[8]","a[9]","a[10]","a[11]","a[12]","a[13]","a[14]","a[15]","a[16]","a[17]","a[18]","a[19]","a[20]","a[21]","a[22]","a[23]","a[24]","a[25]","a[26]","a[27]","a[28]","a[29]","a[30]","a[31]","a[32]","a[33]","a[34]","a[35]","a[36]","a[37]","a[38]","a[39]","a[40]","a[41]","a[42]","a[43]");" }' outSorted.csv >> /tmp/outSorted.sql
echo 'COMMIT;' >> /tmp/outSorted.sql

DROP TABLE verticesIncident;
CREATE TABLE verticesIncident ( 
	id 		BIGINT PRIMARY KEY,
	pos 		GEOMETRY(Point, 4326),
	CrimesAmount 	INTEGER,
	WARRANTS 	INTEGER,
	OTHER_OFFENSES 	INTEGER,
	LARCENY_THEFT 	INTEGER,
	VEHICLE_THEFT 	INTEGER,
	VANDALISM 	INTEGER,
	NON-CRIMINAL 	INTEGER,
	ROBBERY 	INTEGER,
	ASSAULT 	INTEGER,
	WEAPON_LAWS 	INTEGER,
	BURGLARY 	INTEGER,
	SUSPICIOUS_OCC 	INTEGER,
	DRUNKENNESS 	INTEGER,
	FORGERY_COUNTERFEITING INTEGER,
	DRUG_NARCOTIC 	INTEGER,
	STOLEN_PROPERTY INTEGER,
	SECONDARY_CODES INTEGER,
	TRESPASS 	INTEGER,
	MISSING_PERSON 	INTEGER,
	FRAUD 		INTEGER,
	KIDNAPPING 	INTEGER,
	RUNAWAY 	INTEGER,
	DRIVING_UNDER_THE_INFLUENCE INTEGER,
	SEX_OFFENSES__FORCIBLE INTEGER,
	PROSTITUTION 	INTEGER,
	DISORDERLY_CONDUCT INTEGER,
	ARSON 		INTEGER,
	FAMILY_OFFENSES INTEGER,
	LIQUOR_LAWS 	INTEGER,
	BRIBERY 	INTEGER,
	EMBEZZLEMENT 	INTEGER,
	SUICIDE 	INTEGER,
	LOITERING 	INTEGER,
	SEX_OFFENSES__NON_FORCIBLE INTEGER,
	EXTORTION 	INTEGER,
	GAMBLING 	INTEGER,
	BAD_CHECKS 	INTEGER,
	TREA 		INTEGER,
	RECOVERED_VEHICLE INTEGER,
	PORNOGRAPHY_OBSCENE_MAT INTEGER
);
CREATE INDEX verticesIncident_gix ON verticesIncident USING GIST(pos); 

------------------------------------------------------------------------
-- Queries
------------------------------------------------------------------------

Copy (SELECT category AS "Categoria", count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2003 THEN 1 END) AS "2003",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2004 THEN 1 END) AS "2004",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2005 THEN 1 END) AS "2005",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2006 THEN 1 END) AS "2006",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2007 THEN 1 END) AS "2007",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2008 THEN 1 END) AS "2008",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2009 THEN 1 END) AS "2009",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2010 THEN 1 END) AS "2010",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2011 THEN 1 END) AS "2011",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2012 THEN 1 END) AS "2012",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2013 THEN 1 END) AS "2013",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2014 THEN 1 END) AS "2014",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2015 THEN 1 END) AS "2015",
		count(CASE WHEN EXTRACT(YEAR FROM dayIncident) = 2016 THEN 1 END) AS "2016",
		count(*) AS "Total"
FROM incident group by category order by 1) To '/home/postgres/test.csv' With CSV DELIMITER ',' HEADER;


Copy (SELECT category AS "Categoria", count(CASE WHEN dayofweek = 'Sunday' AND (EXTRACT(HOUR FROM dayIncident) > 18 OR EXTRACT(HOUR FROM dayIncident) < 6) THEN 1 END) AS "Sunday Night",
	count(CASE WHEN dayofweek = 'Sunday' AND EXTRACT(HOUR FROM dayIncident) between 6 and 18 THEN 1 END) AS "Sunday Day",
	count(CASE WHEN dayofweek = 'Sunday' THEN 1 END) AS "Sunday",
	
	count(CASE WHEN dayofweek = 'Monday' AND (EXTRACT(HOUR FROM dayIncident) > 18 OR EXTRACT(HOUR FROM dayIncident) < 6) THEN 1 END) AS "Monday Night",
	count(CASE WHEN dayofweek = 'Monday' AND EXTRACT(HOUR FROM dayIncident) between 6 and 18 THEN 1 END) AS "Monday Day",
	count(CASE WHEN dayofweek = 'Monday' THEN 1 END) AS "Monday",

	count(CASE WHEN dayofweek = 'Tuesday' AND (EXTRACT(HOUR FROM dayIncident) > 18 OR EXTRACT(HOUR FROM dayIncident) < 6) THEN 1 END) AS "Tuesday Night",
	count(CASE WHEN dayofweek = 'Tuesday' AND EXTRACT(HOUR FROM dayIncident) between 6 and 18 THEN 1 END) AS "Tuesday Day",
	count(CASE WHEN dayofweek = 'Tuesday' THEN 1 END) AS "Tuesday",

	count(CASE WHEN dayofweek = 'Wednesday' AND (EXTRACT(HOUR FROM dayIncident) > 18 OR EXTRACT(HOUR FROM dayIncident) < 6) THEN 1 END) AS "Wednesday Night",
	count(CASE WHEN dayofweek = 'Wednesday' AND EXTRACT(HOUR FROM dayIncident) between 6 and 18 THEN 1 END) AS "Wednesday Day",
	count(CASE WHEN dayofweek = 'Wednesday' THEN 1 END) AS "Wednesday",

	count(CASE WHEN dayofweek = 'Thursday' AND (EXTRACT(HOUR FROM dayIncident) > 18 OR EXTRACT(HOUR FROM dayIncident) < 6) THEN 1 END) AS "Thursday Night",
	count(CASE WHEN dayofweek = 'Thursday' AND EXTRACT(HOUR FROM dayIncident) between 6 and 18 THEN 1 END) AS "Thursday Day",
	count(CASE WHEN dayofweek = 'Thursday' THEN 1 END) AS "Thursday",

	count(CASE WHEN dayofweek = 'Friday' AND (EXTRACT(HOUR FROM dayIncident) > 18 OR EXTRACT(HOUR FROM dayIncident) < 6) THEN 1 END) AS "Friday Night",
	count(CASE WHEN dayofweek = 'Friday' AND EXTRACT(HOUR FROM dayIncident) between 6 and 18 THEN 1 END) AS "Friday Day",
	count(CASE WHEN dayofweek = 'Friday' THEN 1 END) AS "Friday",

	count(CASE WHEN dayofweek = 'Saturday' AND (EXTRACT(HOUR FROM dayIncident) > 18 OR EXTRACT(HOUR FROM dayIncident) < 6) THEN 1 END) AS "Saturday Night",
	count(CASE WHEN dayofweek = 'Saturday' AND EXTRACT(HOUR FROM dayIncident) between 6 and 18 THEN 1 END) AS "Saturday Day",
	count(CASE WHEN dayofweek = 'Saturday' THEN 1 END) AS "Saturday"
FROM incident group by category order by 1) To '/home/postgres/test.csv' With CSV DELIMITER ',' HEADER;


shp2pgsql /tmp/xxx.shp table > /home/postgres/xxx.sql
SELECT UpdateGeometrySRID('police_districts','geom',4326);

DROP TABLE vertices_district;
CREATE TABLE vertices_district ( 
	id 		BIGINT PRIMARY KEY,
	district	varchar(254),
	pos 		GEOMETRY(Point, 4326)
);

INSERT INTO vertices_district (id, district, pos)
	SELECT vertices.id, police_districts.district, vertices.pos FROM vertices, police_districts WHERE district = 'CENTRAL' AND ST_Contains(geom, pos);
INSERT INTO vertices_district (id, district, pos)
	SELECT vertices.id, police_districts.district, vertices.pos FROM vertices, police_districts WHERE district = 'BAYVIEW' AND ST_Contains(geom, pos);
INSERT INTO vertices_district (id, district, pos)
	SELECT vertices.id, police_districts.district, vertices.pos FROM vertices, police_districts WHERE district = 'INGLESIDE' AND ST_Contains(geom, pos);

INSERT INTO vertices_district (id, district, pos)
	SELECT vertices.id, police_districts.district, vertices.pos FROM vertices, police_districts WHERE vertices.id not in(725298140, 369951067, 65343017, 724028371, 724028369, 724028370, 4027455601) and district = 'NORTHERN' AND ST_Contains(geom, pos);

INSERT INTO vertices_district (id, district, pos)
	SELECT vertices.id, police_districts.district, vertices.pos FROM vertices, police_districts WHERE district = 'SOUTHERN' AND ST_Contains(geom, pos);
INSERT INTO vertices_district (id, district, pos)
	SELECT vertices.id, police_districts.district, vertices.pos FROM vertices, police_districts WHERE district = 'MISSION' AND ST_Contains(geom, pos);
INSERT INTO vertices_district (id, district, pos)
	SELECT vertices.id, police_districts.district, vertices.pos FROM vertices, police_districts WHERE district = 'TENDERLOIN' AND ST_Contains(geom, pos);
INSERT INTO vertices_district (id, district, pos)
	SELECT vertices.id, police_districts.district, vertices.pos FROM vertices, police_districts WHERE district = 'PARK' AND ST_Contains(geom, pos);

INSERT INTO vertices_district (id, district, pos)
	SELECT vertices.id, police_districts.district, vertices.pos FROM vertices, police_districts WHERE vertices.id not in(258467538) and district = 'RICHMOND' AND ST_Contains(geom, pos);

INSERT INTO vertices_district (id, district, pos)
	SELECT vertices.id, police_districts.district, vertices.pos FROM vertices, police_districts WHERE vertices.id not in(440322584, 1815483726, 1815483665, 4027288929, 65311193, 440322583, 1815483698, 440322586) and district = 'TARAVAL' AND ST_Contains(geom, pos);

INSERT INTO vertices_district (id, district, pos)
	SELECT id, 'none', vertices.pos FROM vertices where id in (SELECT vertices.id FROM vertices EXCEPT SELECT id from vertices_district);

CREATE INDEX vertices_district_gix ON vertices_district USING GIST(pos); 

update vertices_district set district = 'RICHMOND' where id in(258467538);
update vertices_district set district = 'TARAVAL' where id in(440322584, 1815483726, 1815483665, 4027288929, 65311193, 440322583, 1815483698, 440322586);
update vertices_district set district = 'NORTHERN' where id in(725298140, 369951067, 65343017, 724028371, 724028369, 724028370, 4027455601);
update vertices_district set district = 'PARK' where id in(SELECT id FROM vertices_district WHERE district = 'none' AND ST_DWithin(pos, ST_GeomFromText('POINT(-122.447731 37.782529)', 4326), 0.005));
update vertices_district set district = 'island' where id in(SELECT id FROM vertices_district WHERE ST_DWithin(pos, ST_GeomFromText('POINT(-122.3545 37.8173)', 4326), 0.03));

update vertices_district set district = 'upper_bound' where id in(SELECT id FROM vertices_district WHERE district = 'none' AND ST_Y(pos) > 37.77);
update vertices_district set district = 'lower_bound' where id in(SELECT id FROM vertices_district WHERE district = 'none' AND ST_Y(pos) < 37.72);


------------------------------------------------------------------------
-- Exports
------------------------------------------------------------------------
Copy (select id, district, St_X(pos), St_Y(pos) from vertices_district order by district) To '/home/postgres/vertices_district.csv' With CSV DELIMITER ',';