﻿sudo yum install postgis*

-- Enable PostGIS (includes raster)
CREATE EXTENSION postgis;
-- Enable Topology
CREATE EXTENSION postgis_topology;
-- fuzzy matching needed for Tiger
CREATE EXTENSION fuzzystrmatch;

-- Cidade, com os dados do Gabriel
awk -F, '{if(NR<=1){next} split($0,a,","); print "INSERT INTO vertices(id, pos) VALUES ("a[1]", ST_GeomFromText(\x27POINT("a[3] " " a[2]")\x27, 4326));" }' nodes-file.csv > nodes-file.sql

DROP TABLE vertices;
CREATE TABLE vertices ( 
	id 		INTEGER PRIMARY KEY,
	pos 		GEOMETRY(Point, 4326),
	comunity1	INTEGER,
	qte1		BIGINT,
	comunity2	INTEGER,
	qte2		BIGINT,
	comunity3	INTEGER,
	qte3		BIGINT,
	comunityAll	INTEGER,
	qteAll		BIGINT
);
CREATE INDEX vertices_gix ON vertices USING GIST(pos); 

awk -F, '{if(NR<=1){next} split($0,a,","); print "INSERT INTO edges(startNode, endNode, distance) VALUES ("a[1]", "a[2]", "a[3]");" }' edges-file.csv > edges-file.sql
DROP TABLE edges;
CREATE TABLE edges ( 
	startNode	INTEGER,
	endNode		INTEGER,
	distance	DECIMAL	
);

DROP TABLE streets;
CREATE TABLE streets AS
	SELECT ST_MAKELINE(V1.pos, V2.pos) as pos, edges.distance as dist
	FROM edges JOIN vertices V1 on V1.id = edges.startNode
		JOIN vertices V2 on V2.id = edges.endNode;
CREATE INDEX streets_gix ON streets USING GIST(pos); 


-- Data from METIS GRAPH Files 
-- update vertex weight
awk -F, '{if(NR<=1){next} split($0,a," "); print "UPDATE vertices SET qte1 = "a[1]" WHERE id = "NR-1";" }' assault.graph > assault.sql
awk -F, '{if(NR<=1){next} split($0,a," "); print "UPDATE vertices SET qte2 = "a[1]" WHERE id = "NR-1";" }' non-criminal.graph > non-criminal.sql
awk -F, '{if(NR<=1){next} split($0,a," "); print "UPDATE vertices SET qte3 = "a[1]" WHERE id = "NR-1";" }' theft.graph > theft.sql
awk -F, '{if(NR<=1){next} split($0,a," "); print "UPDATE vertices SET qteAll = "a[1]" WHERE id = "NR-1";" }' top3.graph > top3.sql

-- update vertex cluster number
awk -F, '{ print "UPDATE vertices SET comunity1 = "$0" WHERE id = "NR";" }' assault.cluster > assault.sql
awk -F, '{ print "UPDATE vertices SET comunity2 = "$0" WHERE id = "NR";" }' non-criminal.cluster > non-criminal.sql
awk -F, '{ print "UPDATE vertices SET comunity3 = "$0" WHERE id = "NR";" }' theft.cluster > theft.sql
awk -F, '{ print "UPDATE vertices SET comunityAll = "$0" WHERE id = "NR";" }' top3.cluster > top3.sql

-- export data
Copy (SELECT id, ST_Y(pos) AS lat, ST_X(pos) AS long, comunity1, qte1, comunity2, qte2, comunity3, qte3, comunityAll, qteAll FROM vertices ORDER BY id) To '/tmp/nodes_clustered.csv' With CSV DELIMITER ',' HEADER;

-- generate adj list
select id, string_agg(string_agg, ' ') from
(SELECT id, string_agg( CAST(concat(endnode, ' ',CAST(distance AS INTEGER) ) AS text), ' ' ) FROM vertices LEFT JOIN edges ON id = startnode GROUP BY id
UNION ALL
SELECT id, string_agg( CAST(concat(startnode, ' ',CAST(distance AS INTEGER) ) AS text), ' ' ) FROM vertices LEFT JOIN edges ON id = endnode GROUP BY id) AS new
GROUP BY id;


-- categories 1 and 2
SELECT distances.comunity1, distances.comunity2, (distComunity - MIN(distComunity) OVER() ) / (MAX(distComunity) OVER() - MIN(distComunity) OVER()) as normDist FROM
(SELECT v1.comunity1, v2.comunity2, avg( ST_Distance_Sphere(v1.pos, v2.pos) ) as distComunity
FROM vertices v1, vertices v2
WHERE v1.comunity1 IN (108, 106, 105, 175, 61) AND
	v2.comunity2 IN (115, 112, 114, 178, 7)
GROUP BY v1.comunity1, v2.comunity2
ORDER BY v1.comunity1, v2.comunity2) AS distances;

-- categories 1 and 3
SELECT distances.comunity1, distances.comunity3, (distComunity - MIN(distComunity) OVER() ) / (MAX(distComunity) OVER() - MIN(distComunity) OVER()) as normDist FROM
(SELECT v1.comunity1, v2.comunity3, avg( ST_Distance_Sphere(v1.pos, v2.pos) ) as distComunity
FROM vertices v1, vertices v2
WHERE v1.comunity1 IN (108, 106, 105, 175, 61) AND
	v2.comunity3 IN (11, 113, 7, 116, 114)
GROUP BY v1.comunity1, v2.comunity3
ORDER BY v1.comunity1, v2.comunity3) AS distances;

-- categories 2 and 3
SELECT distances.comunity2, distances.comunity3, (distComunity - MIN(distComunity) OVER() ) / (MAX(distComunity) OVER() - MIN(distComunity) OVER()) as normDist FROM
(SELECT v1.comunity2, v2.comunity3, avg( ST_Distance_Sphere(v1.pos, v2.pos) ) as distComunity
FROM vertices v1, vertices v2
WHERE v1.comunity2 IN (115, 112, 114, 178, 7) AND
	v2.comunity3 IN (11, 113, 7, 116, 114)
GROUP BY v1.comunity2, v2.comunity3
ORDER BY v1.comunity2, v2.comunity3) AS distances;

-- adding analisys involving communities detection without weights
create table vertices2 as select *, CAST(null as integer) as communityWithoutCrimes from vertices;
awk -F, '{ print "UPDATE vertices2 SET communityWithoutCrimes = "$0" WHERE id = "NR";" }' all.cluster > all.sql

-- zonas criminais
drop table criminalzone1;
create table criminalzone1 as select comunity1, ST_ConcaveHull(st_collect(pos), 0.1) from vertices where comunity1 IN (108, 106, 105, 175, 61) group by comunity1;
drop table criminalzone2;
create table criminalzone2 as select comunity2, ST_ConcaveHull(st_collect(pos), 0.1) from vertices where comunity2 IN (115, 112, 114, 178, 7) group by comunity2;
drop table criminalzone3;
create table criminalzone3 as select comunity3, ST_ConcaveHull(st_collect(pos), 0.1) from vertices where comunity3 IN (11, 113, 7, 116, 114) group by comunity3;

select comunity1, ST_AREA(st_concavehull, false)/1000000 FROM criminalzone1;
select comunity2, ST_AREA(st_concavehull, false)/1000000 FROM criminalzone2;
select comunity3, ST_AREA(st_concavehull, false)/1000000 FROM criminalzone3;

select comunity1, sum(qte1) from vertices where comunity1 IN (108, 106, 105, 175, 61) group by comunity1 order by 1;
select comunity2, sum(qte2) from vertices where comunity2 IN (115, 112, 114, 178, 7) group by comunity2 order by 1;
select comunity3, sum(qte3) from vertices where comunity3 IN (11, 113, 7, 116, 114) group by comunity3 order by 1;



