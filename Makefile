postgres_user=marcus
postgres_create_schema=./postgres/create_schema.sql
postgres_data_base=postgres

crimes_to_be_mapped=../data/"Data Set"/violentCrimes.txt
crimes_data_set=../data/"Data Set"/crimes.csv

vertices_file=../data/"Data Set"/san-francisco_noisy-vertices.txt
edges_file=../data/"Data Set"/san-francisco_noisy-edges.txt

output_mapping_file=outputTEMPORAL.csv

mapping_technique=GRAPH
granularity=MONTH
TimeSeries_begDate=01/01/2003 
TimeSeries_endDate=12/01/2015

mapping_log_file=nohup_output.txt



compile_mapping:
	rm -r temp
	mkdir temp
	rm -r running 
	mkdir running 
	g++ ./code/TimeSeriesApproach/lib/*.h 
	g++ ./code/TimeSeriesApproach/src/*.cpp -c 
	mv *.o ./temp
	g++ ./code/TimeSeriesApproach/src/main.cpp ./temp/node_sf.o ./temp/nodeMatrix_sf.o ./temp/sparseMatrix_sf.o ./temp/csvCrimeFile_sf.o ./temp/graph_sf.o ./temp/nodeGraph_sf.o  -o ./running/main 


run_mapping_with_TimeSeries_approach:
	cd running ; nohup ./main $(crimes_to_be_mapped) $(crimes_data_set) $(vertices_file) $(edges_file) $(output_mapping_file) $(mapping_technique) $(granularity) $(TimeSeries_begDate) $(TimeSeries_endDate) > $(mapping_log_file)  & 
	cd ..
	
clean:
	rm -r ./temp
	rm ./code/TimeSeriesApproach/*.h.gch



create_table:
	psql --file=$(postgres_create_schema) $(postgres_data_base) $(postgres_user) 
