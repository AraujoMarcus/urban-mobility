function d = euclidean(Sp, Sq)
	if (size(Sq)~= size(Sp))
		d=-1;
	else
    	[l,c] = size (Sq);
   	 d=0;
   	 for i =1:c 
      		d= d + (Sq(i)-Sp(i))**2;	
    	 end
    d = d/(c-1);
    d = d**0.5;
  end


end 
