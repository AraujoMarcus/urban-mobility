c= csvread("data.csv");

ts = generateTimeSeries(c, 40);

[l,~] = size(ts);

for i =1: l
	dlmwrite("tsMatrix.csv", ts(i,:), '-append');
end
