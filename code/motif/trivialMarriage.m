function t = trivialMarriage(ts,i,j) % ts is the time series matrix, i and j are the i-th and j-th time series, such that i <= j 
  d = euclidean(ts(i,:), ts(j,:));
  
  t=1;
  for k = i+1:j-1
     if(d< euclidean(ts(i,:), ts(k,:)))
         t=0;
         break;
     end     
  end 
    


end
