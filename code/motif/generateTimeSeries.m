function ts = generateTimeSeries(raw, length)

[lineAmount, ~] = size(raw);
ts = [];

 for (i = 1 : lineAmount - length + 1)
	ts = [ts; raw(i:i+length-1,1)'];

	if(mod(i,1000) ==0) i end 
	
	
 endfor 	




endfunction
