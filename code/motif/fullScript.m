

% input parameters
timeSeriesLength = 40;
motif_radius = 7.2541e-04; 


raw_data = csvread("data.csv");

ts = generateTimeSeries(raw_data, timeSeriesLength ); % time series matrix;

[l,~] = size(ts);

for i =1 : l
	dlmwrite("tsMatrix.csv", ts(i,:), '-append');
end

motif = MFmotif(ts, motif_radius); % find 1-motif most frequent 

[l,~] = size(motif);

for i =1 : l
        dlmwrite("most_frequent_motif.csv", motif(i,:), '-append');
end


[up, down, stable]=Homogeneity(ts,motif) % is it pure 



