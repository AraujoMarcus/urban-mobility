function motif = MFmotif(ts, k)
[tuplesAmount, length] = size(ts);
count =0;
motif = [];
	for i=1: tuplesAmount-1
		tempCount = 0;
		tempMotif = [];
		if(mod(i,1000) ==0) i end 
			trivial=1;
		tempCount= tempCount+1;
               tempMotif = [tempMotif; [i,ts(i,:)]];

		for j=i:tuplesAmount

			eDistance = euclidean(ts(i,:), ts(j,:));
			

			if(trivial ==1)
				trivial = trivialMarriage(ts, i,j);
			end
			if(eDistance == -1)
				printf("Error: different length time series");
			else
				if(trivial==0 && eDistance<=k)
					
					tempCount= tempCount+1;
					tempMotif = [tempMotif; [j,ts(j,:)]];
				end	
			end 	
		end

		if(tempCount > count )
			count = tempCount
			motif = tempMotif;
		end

	end


end 
