% Load data
datapath = './data/';

M = dlmread(strcat(datapath, 'TimeSeries.csv'),',');
classes = dlmread(strcat(datapath, 'class.csv'));

% Get number of columns
cols = size(M, 2);

% Compute PCA
[~,score,~] = princomp(M);

% Do not show the output plot
f = figure('Visible','off');

% Plot 1st and 2nd components of each class value
scatter(score(classes == 0, 1), score(classes == 0, 2), 'blue', '.');
hold on;
scatter(score(classes == 1, 1), score(classes == 1, 2), 'red', '.');
hold on;


% Create and organize labels
%xlabel('1st Principal Component');
%ylabel('2nd Principal Component');
%hLeg = legend('building', 'people', 'vehicles', 'cbsnews');
%set(hLeg, 'Location', 'northoutside');
%set(hLeg, 'Orientation', 'horizontal');

box on;

% Save output image into output path
outputpath = '../figures/pca_selectedKeyframes_allClasses.pdf';
print('-depsc2','-r1080', outputpath);
