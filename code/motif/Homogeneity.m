function [up,down,stable] = Homogeneity(ts,motif)
	up = down = stable =0;
	[l,~] = size(motif);
	for i=1:l
		label= ts(motif(i,1)+1,1);
		if(label ==0)
			stable = stable +1;
		elseif(label <0)
			down = down+1;
		else 
			up = up +1;
		end
	end

end

