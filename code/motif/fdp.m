ts = csvread("data.csv");
[l,c]=size(ts);
n = 50;
dist = [];

for i=1:l-n-1
	i
  minor = 10e20;
  for j=i:l-n
    if(trivialMarriage(ts,n,i,j)==0)
       current= euclidean(ts(i:i+n-1), ts(j:j+n-1));
        if(current < minor )
            minor = current;
        end
    end
   end
   if(minor~= 10e20)
      	  minor  
	   dlmwrite("dist_nonTrivialMarriage.csv", minor, '-append');
      dist = [dist, minor];
  end
  
end

dist = sort(dist);
plot(dist);
