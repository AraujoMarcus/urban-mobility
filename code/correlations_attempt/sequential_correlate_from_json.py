import json 
from scipy.stats.stats import pearsonr 
import sys 
import numpy as np 
import dtw 
import matplotlib.pyplot as plt 
def read_from_file(filepath):

    fp = open(filepath)

    json_array = []

    line = fp.readline()

    while(len(line)>0):
        
        data = json.loads(line)

        json_array.append(data)

        line = fp.readline()

        

    return json_array

def take_measures(json_array, filepath):

    fp = open(filepath.replace(".json", "_dtw_pearson.csv"), "w")

    dtw_global = 0

    success = 0

    failed = 0

    result_global = 0

    aux = 0

    weight_global = len ( json_array )



    for node in json_array:

        ts = list(node['my_TimeSeries'])
    
        weight_partial = len(node['my_neighbors'])
       
        #print("node: " + str(node['rowID']) +" has " + str(weight_partial) + "neighbors")
        result_partial = 0
    
        dtw_partial = 0

        for neighbor in node['my_neighbors']:

            ts_neighbor = list( neighbor['TimeSeries'] )
            
            try:
        
                aux = dtw.dtw(ts, ts_neighbor)[0]

                dtw_partial = dtw_partial + aux

                aux = pearsonr(ts, ts_neighbor)[0]
                
                if(np.isnan(aux)==False):
                
                    result_partial = float(aux) + result_partial
                
                success = success + 1
           
            except:
                
                failed = failed + 1
               
                raise
            
        fp.write(str(node['rowID'])+","+str(dtw_partial/weight_partial)+","+ str(result_partial/weight_partial)+"\n")
        dtw_partial = dtw_partial / (weight_partial*weight_global)

        dtw_global = dtw_partial + dtw_global
        
        result_partial = result_partial / (weight_partial*weight_global)
        
        result_global = result_partial + result_global
    
    print("\tsuccess: " + str(success))
    print("\tfailed: " + str(failed))
    print("\tcorrelation: " + str(result_global))
    print("\tdtw: "+ str(dtw_global))
    fp.close()
    return [result_global, dtw_global]




x_axis = []
pearson_vector = []
dtw_vector = []

for i in range (3,4):

    x_axis.append(i)


    #filepath = "../../results/CorrelationTempResults/neighborhood/" + str(i) + "_level.json"
    filepath = "./" + str(i) + "_level.json"

    data = read_from_file(filepath)

    [PearsonCorrelation_mean,DTW_mean]=take_measures(data, filepath)

    pearson_vector.append(PearsonCorrelation_mean)
    dtw_vector.append(DTW_mean)

#fig = plt.figure()
    
#a1 = fig.add_subplot(211)
#a2 = fig.add_subplot(221)
    
#a1.set_title("pearson measures")
#a2.set_title("dtw_measures")
    
#a1.plot(x, pearson_vector)
#a2.plot(y, dtw_vector)

#plt.show()

    

