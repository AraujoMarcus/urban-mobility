import networkx as nx
import sys 
from scipy.stats.stats import pearsonr 
import matplotlib.pyplot as plt 
import numpy as np
from dtw import dtw

def HNeighborhood(oxG, nID, level):
    visited  = set([nID])
    to_visit = set([nID])
    
    neighborhood = []
   
    if level == 0:
        return neighborhood
    
    for i in xrange(1, level + 1):
        
        if len(to_visit) == 0:
            return neighborhood
        
        #print to_visit, visited
        
        neighborhood.append(to_visit)
        
        discovered = []
        for vID in to_visit:
            discovered = discovered + oxG.neighbors(vID)
        #print 'Discovered', discovered
        
        to_visit = set(discovered) - (to_visit | visited)
        #print 'To Visit', to_visit
        
        visited  = set(discovered) | (to_visit | visited)
        #print 'Visited', visited
    
	
    return neighborhood
            


def importTS(filename):
    # --- file 1: time series  
    
    dic = {}

    fp = open(filename)
    
    #header
    line = (fp.readline()).replace('\n', '').split(',') 
    
    ts_beg=0;
    while('TimeSeries' != line[ts_beg]):
        ts_beg = ts_beg + 1
    

    #processing lines and computing {id+ts}
    line = fp.readline() 

    while(len(line)>0):

        aux = line.split(',');

        values = []
         
        for i in range(ts_beg, len(aux)):
        
            values.append(float(aux[i]))
    
        dic.update({ aux[0]:values})
        

        line = fp.readline()

    
    fp.close() 
    
    return dic


def getNotNullElements(ts):

    sample = []

    id_ = list(ts)

    for current in id_:

        if(sum(ts[current]) >0 ):
            
            sample.append(current)

    return sample 

        
def corr (ts, node, neighbors):

     

    result = 0.0
    
    for i in range (0, len(neighbors)):
        
       # print("node : "+ str(node)+ "\n")
       # print("neighbor :" +str(neighbors[i]) +"\n")
        aux = pearsonr(ts[str(node)], ts[str(neighbors[i])])[0]

        if(np.isnan(aux)==False):
            result = result + aux

    result = result / len(neighbors)
    
   
    return result

def dtw_distance (ts, node, neighbors):



    result = 0.0

    for i in range (0, len(neighbors)):

        result = result + dtw(ts[str(node)], ts[str(neighbors[i])])[0]

    result = result / len(neighbors)





    return result


def mount_neighborhood(ts, G,  nodes):

	


    #generate list of neighborhs to all nodes 
    
    print("building neighborhood...\n")

    neighborhood = []
    count =0.0 
    for node in nodes:
        print(str(count/len(nodes)))
        count= count+1
        if(np.sum(ts[str(node)])>0):
            neighborhood.append(list(HNeighborhood(G, node, 100)))
        else:
            neighborhood.append([])
    
    
    for j in range (2, 100):
        
        print("level " + str(j) +"...")
        fp = open("../../results/temp_results/"+str(j)+"_level.json", "w")
             
          
        for i in range (0, len(nodes)):
        
            row = {} 
        
            if(np.sum(ts[str(nodes[i])])>0):

                row.update({"rowID":nodes[i]})
                row.update({"my_TimeSeries":ts[str(nodes[i])]})
                
                my_neighbors_rows = []

                neighbors = list(neighborhood[i])
    
                if(len(neighbors) >= j):
    
                    for neighbor in list(neighbors[j-1]):
                
                        neighbor_row = {}

                        neighbor_row.update({"rowID":neighbor, "TimeSeries":ts[str(neighbor)]})

                        my_neighbors_rows.append(neighbor_row)

                    row.update({"my_neighbors":my_neighbors_rows})

            
                    fp.write(str(row).replace("\'", "\"")+"\n")

        fp.close()
        print("\tdone!")                    

            
            
    

def main():
        filepath = sys.argv
        print(str(filepath))
	# input : [nodes_file, edges_file]
        if (len (filepath) < 2) :
	    print ("Error: input Nodes and Edges filepath")
	else:
	    
	    G = nx.Graph()

	    #1. ------------------nodes file reading-------------------
	    
	    f = open(filepath[1], 'r')
	    
	    

	    # 1.1 Header
	    
	    header = []
	    
	    flag = True
	    
	    while(flag):
	    
		aux = f.readline()
		    
		if(aux[0].find('#')==-1):
		
		    flag = False
		
		else:
		
		    header.append(aux) 
	    
	    # 1.2 Nodes 
	    
	    flag = True
	    
	    nodes = {} 
	    
	    id_index = []

	    while(flag):

		if(len(aux) > 0):
	    
		    mydata = aux.split(' ')
		    
                    id_index.append(int(mydata[0]))

		    nodes.update ({int(mydata[0]): (float(mydata[1]), float(mydata[2]))})
		    
		    G.add_node(int(mydata[0]))
		    
		    aux = f.readline()
	       
		else:
		   flag = False
	       
	   
	    f.close()
	    
	    # 2. ---------------- Edges file reading ------------------
	    
	    f = open (filepath[2], 'r')
	    
	    
	    # 2.1 Header

	    header = []
	    
	    flag = True
	    
	    while(flag):

		aux = f.readline()
		
		if(aux[0].find('#')==-1):
		    
		    flag = False
		
		else:
		    header.append(aux)
		    

	    # 2.2 Edges
	    
	    flag = True
	    
	    edges = {}
	    
	    while(flag):

		if(len(aux) > 0):
	    
		    mydata = aux.split(' ')
		    
		    edges.update ({float(mydata[2]): (int(mydata[0]), int(mydata[1]))})
		    
		    G.add_edge(int(mydata[0]), int(mydata[1]))
		    
		    aux = f.readline()
		
		else:
		    flag = False

	    f.close()


	    #3. ----------------   Drawing  ----------------------------

     

	    #nx.draw_networkx_nodes(G,nodes,node_size=20,node_color='b')

	    #nx.draw_networkx_edges(G,nodes) 
	   
	    #plt.show()


	    #4. ------ analysis ---- 
	    

	    ts = importTS(filepath[3])
		

	    return [ts, G, id_index]

[ts, G, id_index] = main()

mount_neighborhood(ts, G, id_index)
