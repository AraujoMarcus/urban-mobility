# the volume is bigger than i can wait to process, then I extracted sample from hotspot 



def extract_ts_sample (inFilePath, outFilePath):

    min_x = 37.764756
    max_x = 37.813506
        
    min_y = -122.44795
    max_y = -122.380476
    
    inputFile = open(inFilePath, 'r')
    outputFile = open(outFilePath, 'w')
    
    #ignore header
    inputFile.readline() 
    
    line = inputFile.readline()
    

    while(len(line)>0):

        aux = line.split(',')
        x = float(aux[1])
        y = float(aux[2])

        if((x>=min_x and x<=max_x)and(y>=min_y and y<=max_y)):
            outputFile.write(line)

        line = inputFile.readline()
                    
    
    inputFile.close()
    outputFile.close()

def extract_nodes_sample (inFile, outFile):

    min_x = 37.764756
    max_x = 37.813506

    min_y = -122.44795
    max_y = -122.380476

    sample_ids =[]

    inputfile = open(inFile)
    outputfile = open(outFile, 'w')

    line = inputfile.readline()
    while(line[0]=='#'):
        outputfile.write(line)    
        line = inputfile.readline()

    while(len(line)>0):
            
        aux = line.split(' ')
        
        x = float(aux[1])
        y = float(aux[2])

        if(x>=min_x and x<=max_x and y>=min_y and y<=max_y):
            outputfile.write(line)
            sample_ids.append(aux[0])
        line = inputfile.readline()

    outputfile.close()
    inputfile.close()
    
    return sample_ids
    
def extract_edges_sample(sample , infile , outfile):
    inputfile = open(infile)
    outputfile = open(outfile, 'w')

    line = inputfile.readline()
    
    while(line[0]=='#'):
        outputfile.write(line)
        line = inputfile.readline()
    
    while(len(line)>0):

        aux = line.split(' ')
        flag =0
        for i in sample: 
            if(aux[0] == i):
                flag = flag +1
            if(aux[1] == i):
                flag = flag +1

        if(flag ==2):

            outputfile.write(line)
        line = inputfile.readline()


    return 0
