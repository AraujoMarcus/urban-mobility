import threading 
import parallel_correlate_from_json as measures 
import time
import sys 



class myThread (threading.Thread):
    
    def __init__(self, threadID, level, filepath, beg, end):

        threading.Thread.__init__(self)
        
        self.threadID = threadID
        
        self.level = level

        self.filepath = filepath

        self.beg = beg 

        self.end = end 

        self.dtw = 0.0
        self.pearson = 0.0
    
    def stop(self):

        self._stop_event.set()

    def run(self):
        
        print ("Starting " + self.name)
        
        [self.pearson, self.dtw] = measures.run(self.level, self.filepath, self.beg, self.end)
    
        print("Exiting " + self.name)

        
        
    def is_alive(self):
        
        return self.isAlive()
      
def countLines(filepath):

    fp = open(filepath, "r")
    count =0

    while(len(fp.readline())>0):
        count = count+1

    fp.close()
    return count 

def isDone(threads):

    for thread in threads:
        if(thread.is_alive()):
            return False

    return True

def main():
    
    resultSufix = "_result.json"
    sourceSufix = "_level.json"
    
    dirPath = sys.argv[1]
    
    numCores = int(sys.argv[2])
    
    minLevel = int(sys.argv[3])
    
    maxLevel = int(sys.argv[4])

    for i in range (minLevel, maxLevel +1):
        thread_record = [] 

        filepath = dirPath + str(i) 

        fp = open(filepath+resultSufix, "w")

        n_lines = countLines(filepath+sourceSufix) 
    
        if(n_lines<numCores):
            numCores =n_lines
            print("more cores than lines! numThreads decreased to " +str(n_lines))

        charge = n_lines/numCores

        filepath = filepath + sourceSufix

        for j in range (0, numCores):
         
            if(j < numCores-1):

                beg_line = j*(n_lines / numCores)
                end_line = (j+1)*(n_lines/numCores) - 1 
            else:
                beg_line = end_line + 1
                end_line = n_lines
           
            
            print("thread " + str(j) + " takes from " + str(beg_line) + " to " + str(end_line))

    
            thread_record.append(myThread(j,i,filepath, beg_line, end_line))

            thread_record[j].start()

        while(isDone(thread_record) ==  False ):  
            time.sleep(2) 

        

        final_dtw=0.0
        final_pearson = 0.0
        for thread in thread_record:
            final_dtw = final_dtw + thread.dtw
            final_pearson = final_pearson + thread.pearson


        final_dtw = final_dtw / n_lines
        final_pearson = final_pearson / n_lines

        print(str(i) + " done!")
        
        fp.write("{\"level\": "+str(j)+", \"dtw\":"+str(final_dtw)+", \"pearson\": "+str(final_pearson)+"}\n")
        
        
        fp.close()

if __name__ == "__main__":
    main()
