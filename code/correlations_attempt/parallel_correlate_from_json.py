import json 
from scipy.stats.stats import pearsonr 
import sys 
import numpy as np 
import dtw 

def read_from_file(filepath):

    fp = open(filepath)

    json_array = []

    line = fp.readline()

    while(len(line)>0):
        
        data = json.loads(line)

        json_array.append(data)

        line = fp.readline()

        

    return json_array

def run(Neigh_level, sourceFile, beg, end):


    dtw_global = 0

    success = 0

    failed = 0

    pearson_global = 0

    aux = 0

    weight_global = ( end - beg )

    fp = open(sourceFile, "r")
    currentLine = 0
    
    while(currentLine < (beg )):

        if(len(fp.readline())>0):

            currentLine = currentLine + 1 

        else:
            return [-100,-100]

    
    for currentLine in range(beg, end+1):

        line = fp.readline()
    
    
        if(len(line)==0):
            break;

        node = json.loads(line)

        ts = list(node['my_TimeSeries'])
         
        weight_partial = len(node['my_neighbors'])
        

        #print("node: " + str(node['rowID']) +" has " + str(weight_partial) + "neighbors")

        result_partial = 0
    
        dtw_partial = 0

        max_dtw = -10e20
        max_pearson = -10e20
        node_max = [-1,-1]

        for neighbor in node['my_neighbors']:

            ts_neighbor = list( neighbor['TimeSeries'] )
            
            try:
        
                aux = dtw.dtw(ts, ts_neighbor)[0]
                
                if(aux>max_dtw):
                    max_dtw = aux
                    node_max[0] = neighbor['rowID']


                dtw_partial = dtw_partial + aux
    
                aux = pearsonr(ts, ts_neighbor)[0]

                if(np.isnan(aux)==False):
                
                    if(aux > max_pearson):
                        max_pearson = aux 
                        node_max[1] = neighbor['rowID']

                    result_partial = float(aux) + result_partial
                
                success = success + 1
           
            except:
                
                failed = failed + 1
               
                raise
        
        if(max_dtw != -1):
        
            dtw_partial = dtw_partial / (weight_partial)

            dtw_global = dtw_partial + dtw_global
        
            result_partial = result_partial / (weight_partial)
        
            pearson_global = result_partial + pearson_global
    
            if(max_pearson == -10e20):
            
                print(str(Neigh_level)+","+str(node['rowID'])+","+str(dtw_partial)+","+str(result_partial)+","+str(node_max[0])+","+str(max_dtw)+",,")
            
            else:
                
                print(str(Neigh_level)+","+str(node['rowID'])+","+str(dtw_partial)+","+str(result_partial)+","+str(node_max[0])+","+str(max_dtw)+","+str(node_max[1])+","+str(max_pearson))

#    print("lines: "+ str(currentLine))    
#    print("\tsuccess: " + str(success))
#    print("\tfailed: " + str(failed))
#    print("\tcorrelation: " + str(pearson_global))
#    print("\tdtw: "+ str(dtw_global))
    fp.close()
    return [pearson_global, dtw_global]





    

