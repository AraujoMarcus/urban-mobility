#ifndef sparseMatrix_sf_h
#define sparseMatrix_sf_h
#include<vector>
#include<algorithm>
#include<iostream>
#include<stdlib.h>
#include<string.h>
#include "nodeMatrix_sf.h"
#include "csvCrimeFile_sf.h"
using namespace std;

class sparseMatrix_sf{

	private:
		nodeMatrix_sf ** line, **column;
		int latSize, lonSize;
		double * lat;
		double * lon;
		void quick_sort(double *v, int beg, int end);
		int mediana (double * candidate, int *local);
		
	public:

		double EsfericEuclideanDistance(double x1, double y1, double x2, double y2);
		double EuclideanDistance(double x1, double y1, double x2, double y2);
		int BinarySearch(double * array, double value, int beg, int end);
		sparseMatrix_sf(node_sf ** nodes, int amount);
		~sparseMatrix_sf();
		nodeMatrix_sf * getElemAt(double lat, double lon);
		bool add(node_sf*node);
		nodeMatrix_sf * find(int x, int y);
		void collectNodes(nodeMatrix_sf * source, nodeMatrix_sf * current , const char * direction, double range, nodeMatrix_sf ** list,  int * count);
		//bool generateRetangle(double range, nodeMatrix_sf* elem, nodeMatrix_sf *** list, int *amount);
		bool generateRetangle(double range, int x, int y, nodeMatrix_sf *** list, int *amount);
		double EsfericEuclideanDistance(nodeMatrix_sf * a, nodeMatrix_sf * b);

		double EsfericEuclideanDistance(double x1, double y1, nodeMatrix_sf * a);
		pair <int, int> SequencialSearch(double lat, double lon);
		pair <int, int> ClosestValue(double lat, double lon);
		void test();
		void Classification(csvCrimeFile_sf * header, double range);
		void BinarySearch(double * v , double value, int beg, int end, double *dist, int *bestfit);
		void ApplyAverageOnRange(double range, csvCrimeFile_sf * header);	
		
	



};


#endif
