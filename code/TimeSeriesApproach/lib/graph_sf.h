#ifndef graph_sf__h
#define graph_sf__h
	
#include "nodeGraph_sf.h"
	

class graph_sf{ // linked list 
	private:
		int size;
		int edgesAmount;
		nodeGraph_sf ** list;
		node_sf ** nodes;
		
		void Myquick_sort(node_sf ** nodes, int beg, int end );
		int mediana(double * candidates , int* local);
		char * readLine(FILE * fp);
		int CheckNeighborhood(double ** update, int i, int k);
	public:

		graph_sf(node_sf ** nodes, int NodesAmount);
		~graph_sf();
		int addEdge(double source, double destiny, double length);
		void orderList();
		node_sf *getNodeById(double id, int beg, int end, int * ref);
		void loadEdges(char * filepath);
		int getEdgesAmount();
		void knn_soft();
		void report(char * filepath);
};		



#endif