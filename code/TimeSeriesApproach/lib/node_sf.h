#ifndef node_sf_h
#define node_sf_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

using namespace std;


class node_sf{
	private: 
		double id;
		double lat, lon;
		double AbsoluteCrimeCounter;
		double *CrimesCounter;
		int PossibleCrimesAmount;
		int * TimeSeries;	
		int TimeSeriesLength;	
	public:
		
		node_sf();
		node_sf(double id,double lat, double lon, double AbsoluteCrimeCounter,
		double *CrimesCounter);
		node_sf(double id,double lat, double lon, double AbsoluteCrimeCounter,
		double *CrimesCounter, int TimeSeriesLength);
		 ~node_sf();
		 double getId();
		 double getLat();
		 double getLon();
		 double getTotalCrimes();
		 double getCrimesOfClass(int id_crime);
		 int getPossibleCrimesAmount();
		 void setTotalCrimes(double value);
		 void incrementTotalCrimesCounter();
		 void incrementCrimeCounterOfClass(int id_crime);
		 void setCrimesOfClass(int id_crimes, double  value);
		 void setCrimesCounter(double * Counters);
		 void setPossibleCrimesAmount(int value);
		 void InitializeCounters();
		
		 
		void AddToTimeSeries(char * date, int TSgranularity, int initialYear);
		int * getTimeSeries();
		int getTimeSeriesLength();
		int CalculatePositionOnTS(int date, int beg, int granularity);
		int DateTransformation(char *date);
};	

#endif 
