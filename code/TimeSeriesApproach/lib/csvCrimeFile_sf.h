#ifndef csvCrimeFile_sf__h
#define csvCrimeFile_sf__h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define pi 3.14159265359



/*
typedef struct {
	char ** variable;
	int size;
}header;


typedef struct{ 
	char **values;
}data;


typedef struct{
	data ** ds;
	int size;
}dataset;


typedef struct {
	double id;
	double lat, lon;
	int amountCrimes;
	int * amountCrimesClass;
}node;	
*/

using namespace std;

class csvCrimeFile_sf{

	private:
		char ** header_variable;
		int size_header;
		char ** data_values;
		char ** CrimesList;
		int typeOfCrimes;
		FILE * CrimeFile;
	public:
		~csvCrimeFile_sf();
		csvCrimeFile_sf(FILE *fp, char * crimesFile);
		char ** readCSV(FILE *fp, int * size);
		char * getValue(int id);
		int getVariablesAmount();
		char * getVariable(int id);
		char * getCrimeName(int id_crime);
		char ** getCrimesList(char * crimesFile);
		int getTypeOfCrimes();
		void printHeader();
		char * readLine(FILE *fp);
		int LoadNextTuple();
		void destroyTuple();
		void LoadCrimeTypes(char * crimesFile);
		void setCrimeFile(FILE *fp);
		FILE * getCrimeFile();
		bool isTupleEmpty();

		
};

#endif
