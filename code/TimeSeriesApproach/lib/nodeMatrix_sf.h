#ifndef nodeMatrix_sf__h
#define nodeMatrix_sf__h

#include "node_sf.h"
#include <vector>
using namespace std;

class nodeMatrix_sf{
	private:
		node_sf * node;
		nodeMatrix_sf *s, *n, *e, *w; // possible directions	
		double lat;
		double lon;

		int neighbors;
	public:
		nodeMatrix_sf();
		nodeMatrix_sf(node_sf * node);
		~nodeMatrix_sf();
		nodeMatrix_sf * getNorth();
		nodeMatrix_sf * getSouth();
		nodeMatrix_sf * getEast();
		nodeMatrix_sf * getWest();
		nodeMatrix_sf ** getNorthWest(int range);
		nodeMatrix_sf ** getNorthEast(int range);
		nodeMatrix_sf ** getSouthWest(int range);
		nodeMatrix_sf ** getSouthEast(int range);
		void setNorth(nodeMatrix_sf*n);
		void setSouth(nodeMatrix_sf*s);
		void setEast(nodeMatrix_sf*e);
		void setWest(nodeMatrix_sf*w);
		void setNeighborsAmount(int value);
		node_sf* getNode();
		double getNodeId();
		double getLat();
		double getLon();
		int getNeighborsAmount();
};
	

#endif 
