#ifndef nodeGraph_sf__h
#define nodeGraph_sf__h

#include "node_sf.h"

using namespace std;

class nodeGraph_sf{
	private:
		node_sf * node;
		nodeGraph_sf *prev, * next;	
		double length;	
		
		

	public:
		nodeGraph_sf(node_sf * node);
		~nodeGraph_sf();
		void setPrev(nodeGraph_sf* node);
		void setNext(nodeGraph_sf* node);
		void setLength(double length);
		double getLength();
		nodeGraph_sf * getPrev();
		nodeGraph_sf * getNext();
		node_sf* getNode();
		double getNodeId();


};
	

#endif 
