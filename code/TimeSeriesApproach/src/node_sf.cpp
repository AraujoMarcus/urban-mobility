#include "../lib/node_sf.h"

#define LOW_LIMIT 20030101 

node_sf::node_sf(){
	this->id=-1;
	this->CrimesCounter=NULL;
};
node_sf::node_sf(double id,double lat, double lon, double AbsoluteCrimeCounter,
		double *CrimesCounter, int TimeSeriesLength){
		this->id = id;
		this->lat = lat;
		this->lon = lon;
		this->AbsoluteCrimeCounter = AbsoluteCrimeCounter;
		this->CrimesCounter=CrimesCounter;
		this->PossibleCrimesAmount=0;
		this->TimeSeriesLength= TimeSeriesLength;
		this->TimeSeries= (int *)calloc(sizeof(int), TimeSeriesLength);
};

int node_sf::CalculatePositionOnTS(int date, int beg, int granularity){
	
	return (date-beg)/granularity;

};

int node_sf::DateTransformation(char *date){
/*
         *      date format : mm/dd/yyyy
         * */

        int year, month, day, position;

        year = month = day =0;


        /*Split policy*/

        char * p = date;

        while(*p!='/'){
		month*=10;
                month+=((int)*p)-48;
                p++;
        }

        p++;

        while(*p!='/'){
		day*=10;
                day+=((int)*p)-48;
                p++;
        }

        p++;

        while(*p!='\0'){
		year*=10;
                year+=((int)*p) -48;
                p++;
        }


	return year*372 + month *31 + day;

};

void node_sf::AddToTimeSeries(char * date, int TSgranularity, int initialYear){
	
	int newDate = DateTransformation(date); 
	
	int position = CalculatePositionOnTS(newDate, initialYear, TSgranularity);
	

	this->TimeSeries[position]+=1;
	

}

node_sf::node_sf(double id,double lat, double lon, double AbsoluteCrimeCounter,
                double *CrimesCounter){
                this->id = id;
                this->lat = lat;
                this->lon = lon;
                this->AbsoluteCrimeCounter = AbsoluteCrimeCounter;
                this->CrimesCounter=CrimesCounter;
                this->PossibleCrimesAmount=0;
                this->TimeSeries = NULL;
};

int node_sf::getTimeSeriesLength(){
		
	return this->TimeSeriesLength;

}

int * node_sf::getTimeSeries(){

	return this->TimeSeries;

}


node_sf::~node_sf(){
	
	if(this->CrimesCounter!=NULL){
		
		free(this->CrimesCounter);
	}
	if(this->TimeSeries!=NULL){
		free(this->TimeSeries);
	}
	
};

void node_sf::setTotalCrimes(double value){
	this->AbsoluteCrimeCounter=value;
};
void node_sf::incrementTotalCrimesCounter(){
	this->AbsoluteCrimeCounter++;
};
void node_sf::incrementCrimeCounterOfClass(int id_crime){
	this->CrimesCounter[id_crime]++;
};
void node_sf::setCrimesOfClass(int id_crime, double value){
	this->CrimesCounter[id_crime] = value;
};
void node_sf::setCrimesCounter(double * Counters){
	this->CrimesCounter= Counters;
};
void node_sf::InitializeCounters(){
	
	if(this->CrimesCounter!=NULL)
		free(this->CrimesCounter);
	
	this->CrimesCounter = (double*)calloc(sizeof(double),this->PossibleCrimesAmount);


};
void node_sf::setPossibleCrimesAmount(int value){
	this->PossibleCrimesAmount=value;
};

int node_sf::getPossibleCrimesAmount(){
	return this->PossibleCrimesAmount;
};

double node_sf::getId() {
	return this->id;
};
double node_sf::getLat(){
	return this->lat;
};
double node_sf::getLon(){
	return this->lon;
};
double node_sf::getTotalCrimes(){
	return this->AbsoluteCrimeCounter;
};
double node_sf::getCrimesOfClass(int id_crime){
	return this->CrimesCounter[id_crime];
};

