#include "../lib/csvCrimeFile_sf.h"


void csvCrimeFile_sf::LoadCrimeTypes(char * crimesFile){

	if(this->CrimesList!=NULL){
		for(int i=0; i<typeOfCrimes; i++){
			if(this->CrimesList[i]!=NULL)
				free(this->CrimesList[i]);
		}
		free(this->CrimesList);
	}

	FILE * fp=fopen(crimesFile, "r");
	
	
	if(fp==NULL){
		printf("could not find '%s' (file containing all crimes to be analyzed), I'm killing the procedure...\n", crimesFile);
		exit(0);

	}

	char ** crimes=NULL;
	int count =0;
	char * line =NULL;
	do{

		
		line = readLine(fp);
		
		if(line!=NULL){
			crimes=(char**)realloc(crimes, sizeof(char*)*(count+1));
			crimes[count++]=line;
			printf("%d:\t%s\n", count, crimes[count-1]);
		}
	
	}while(line!=NULL);
	fclose(fp);

	this->CrimesList=crimes;
	this->typeOfCrimes=count;
	
}	

int csvCrimeFile_sf::getTypeOfCrimes(){
	return this->typeOfCrimes;
}

bool csvCrimeFile_sf::isTupleEmpty(){
	if(this->data_values==NULL)
		return true;
	else 
		return false;
}


char ** csvCrimeFile_sf::getCrimesList(char * crimesFile){
	if(this->CrimesList==NULL)
		this->LoadCrimeTypes(crimesFile);
	return this->CrimesList;	
};
char * csvCrimeFile_sf::getCrimeName(int id_crime){
	return this->CrimesList[id_crime];
};

char ** csvCrimeFile_sf::readCSV (FILE * fp, int * size){ // read header and save its variables 
	char c;
	char l;
	int line, col=0;
	char ** header=NULL;
	char *  variable=NULL;
	
		do{
			
			
				line =0;
				variable =NULL;

			do{	
				l=fgetc(fp);
				if(l!='\t' && l!=EOF && l!='\n' && l!=','){
					variable = (char *)realloc(variable, sizeof(char)*(line+1));
					variable[line++]= l;
				}else if(line>0){
					variable = (char *)realloc(variable, sizeof(char)*(line+1));
					variable[line++]= '\0';
				}

			}while(l!='\t'&&l!=EOF && l!='\n' && l!=',');

			if(variable!=NULL){
				header= (char**)realloc(header, sizeof(char*)*(col+1));
				header[col++]=variable;
			}
			if(l=='\n'){ // get only first line
				break;
			}

		}while(variable!=NULL);

	
	*size=col;
	return header;
}

csvCrimeFile_sf::csvCrimeFile_sf(FILE *fp, char * crimesFile){
	this->CrimesList=NULL;
	this->typeOfCrimes=0;
	this->data_values=NULL;
	this->CrimesList=NULL;
	this->size_header=0;
	this->CrimeFile=fp;
	this->LoadCrimeTypes(crimesFile);
	this->header_variable= readCSV(fp, &this->size_header);
};

void csvCrimeFile_sf::printHeader(){
	printf("\t%d variables\n", this->size_header);
	
	printf("header = {\n");
	for(int i =0; i<this->size_header; i++){
		printf("\t%s;\n", this->header_variable[i]);
	}
	printf("}\n");	
}



csvCrimeFile_sf::~csvCrimeFile_sf(){
	this->destroyTuple();
	if (this->header_variable!=NULL){
		for(int i=0; i<this->size_header; i++){
			if(this->header_variable[i]!=NULL){
				free(this->header_variable[i]);
			}
		}
		free(this->header_variable);
		this->size_header=0;
	}
	if(this->CrimesList!=NULL){
		for(int i=0; i<this->typeOfCrimes; i++){
			free(this->CrimesList[i]);
		}
		free(this->CrimesList);
		this->CrimesList=NULL;
	}

};

void csvCrimeFile_sf::setCrimeFile(FILE* fp){
	this->CrimeFile=fp;
};
FILE * csvCrimeFile_sf::getCrimeFile(){
	return this->CrimeFile;
};

char *csvCrimeFile_sf::getValue(int id){
	return this->data_values[id];
};
int  csvCrimeFile_sf::getVariablesAmount(){
	return this->size_header;
};
char * csvCrimeFile_sf::getVariable(int id){
	return this->header_variable[id];
};
char * csvCrimeFile_sf::readLine(FILE * fp){ // read an unique line from file 
	char *input =NULL;
	char  c;
	int count =0;
	do{	
		c=fgetc(fp);
		if(c!='\n'&&c!=EOF){
			input=(char*)realloc(input, sizeof(char)*(count+1));	
			input[count++]=c;
		}else if(count>0){
			input=(char*)realloc(input, sizeof(char)*(count+1));	
			input[count++]='\0';
		}
	}while(c!='\n'&&c!=EOF);


	return input;
}

void csvCrimeFile_sf::destroyTuple(){
	if(this->data_values!=NULL){
		for(int i=0; i<this->size_header; i++){
			if(this->data_values[i]!=NULL){
				free(this->data_values[i]);
			}
		}
		free(this->data_values);
		this->data_values=NULL;
	}
}

int csvCrimeFile_sf::LoadNextTuple(){ /*read a crime register from file*/
	/*

		Given a tuple (csv model)
			cross the header model and allocate in proper structure 
		
		input : string (a,"b,c",...)
		output: struct = {val1: a ; val2: b,c;  ... }

	*/		
	


	char * input =NULL;
	int i, j, count ;
	int length;

	input=readLine(this->CrimeFile);


		if(input!=NULL){
 
 			// new register
			if(this->data_values==NULL)
				this->data_values = (char**)malloc(sizeof(char*)*this->size_header);

			i=0; // walk on line 
			j=0; // variable counter 
			
			length=strlen(input);
			
			while (i<length){ // catch all the elements from current line

				if(input[i]==','){ // new variable 
					

					i++; count = 0; // prepare for receiving the next variable 
					
					if(input[i]=='"'){ // variable can support more than one value 
					
						i++;
						this->data_values[j] = NULL;
						while(input[i]!='"' and i<length){ // copy char by char from line 
							this-> data_values [j] = (char *)realloc(this->data_values[j], sizeof(char)*(count+1));
							this-> data_values [j][count++]=input[i++];						
						}
						this->data_values [j] = (char *)realloc(this->data_values[j], sizeof(char)*(count+1));
						this->data_values [j][count++]='\0';	


					
						i++;
					
						
					
						j++;// register ended

					}else{
						
						
						this->data_values[j] = NULL;

						while(input[i]!=',' and i<length ){
						
							this->data_values[j]=(char*)realloc(this->data_values[j], sizeof(char)*(count+1));
						
							this->data_values[j][count++]=input[i++];
						
						}
						this->data_values[j]=(char*)realloc(this->data_values[j], sizeof(char)*(count+1));
						
						this->data_values[j][count++]='\0';

					

						j++;// register ended

					}

				}else{ // (first element)
					count =0;

					this->data_values[j] = NULL;	

					while(input[i]!=',' and i<length){
						
						this->data_values[j] = (char *)realloc(this->data_values[j], sizeof(char)*(count+1));
						this->data_values[j][count++] = input[i++];

					}
					
					this->data_values[j] = (char *)realloc(this->data_values[j], sizeof(char)*(count+1));
					this->data_values[j][count++] = '\0';

					

					j++;// register ended
				}

					
			
			
			}
		}
		if(input!=NULL) free(input);
		if(this->data_values!=NULL)
			return 1;
		else 
			return 0;
}

