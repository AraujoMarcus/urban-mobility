#include "../lib/nodeGraph_sf.h"


using namespace std;


nodeGraph_sf::nodeGraph_sf(node_sf * node){
	this->node= node;
};
nodeGraph_sf::~nodeGraph_sf(){

};
void nodeGraph_sf::setPrev(nodeGraph_sf * node){
	this->prev=node;
};
node_sf * nodeGraph_sf::getNode(){
	return this->node;
}
void nodeGraph_sf::setNext(nodeGraph_sf* node){
	this->next=node;
};
void nodeGraph_sf::setLength(double length){
	this->length=length;
};
double nodeGraph_sf::getLength(){
	return this->length;
};
nodeGraph_sf * nodeGraph_sf::getPrev(){
	return this->prev;
};
nodeGraph_sf * nodeGraph_sf::getNext(){
	return this->next;
};

double nodeGraph_sf::getNodeId(){
	return this->node->getId();
}
