#include "../lib/sparseMatrix_sf.h"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b)) 

using namespace std;

int sparseMatrix_sf::mediana (double * candidate, int *local){

	if(candidate[0]>= candidate[0] && candidate[0]<=candidate[1]) return local[0];
	if(candidate[1]>= candidate[0] && candidate[0]<=candidate[0]) return local[0];

	if(candidate[0]>= candidate[1] && candidate[1]<=candidate[2]) return local[1];
	if(candidate[2]>= candidate[1] && candidate[1]<=candidate[0]) return local[1];

	if(candidate[0]>= candidate[2] && candidate[2]<=candidate[1]) return local[2];
	if(candidate[1]>= candidate[2] && candidate[2]<=candidate[0]) return local[2];


};

void sparseMatrix_sf::quick_sort(double *v, int beg, int end){

	if(end-beg<=0) return ;

	int middle = (end+beg)/2;
	double candidates[3] = {v[beg] ,v[middle], v[end]};
	int local[3] = {beg, middle, end};
	int local_pivot = mediana (candidates, local);
	double pivot = v[local_pivot];
	int i ,j;

	v[local_pivot]=v[end];
	v[end]=pivot;
	i=beg-1;
	j=end;
	
	do{
		do{i++;}while(v[i]<pivot);
		do{j--;}while(v[j]>=pivot && j>=beg);
		if(i<j){
			double aux = v[i];
			v[i]=v[j];
			v[j]=aux;

		}
	
	}while(i<j);	
	v[end]=v[i];
	v[i]=pivot;
	
	quick_sort(v, beg, i-1);
	quick_sort(v, i+1, end);
};


pair <int, int> sparseMatrix_sf::SequencialSearch(double lat, double lon){ // sequencial search
	double latdiff, londiff, aux;
	pair < int , int > position;
	position.second=position.first=-1;
	latdiff=10e20;
	londiff=10e20;

	for(int i=0; i<this->lonSize || i<this->latSize; i++){
		
		if(i<this->latSize){
			aux = (this->lat[i]-lat)*(this->lat[i]-lat);
			if(latdiff > aux){
				latdiff = aux;
				position.first=i;
			}
		}

		if(i<this->lonSize){
			aux = (this->lon[i]-lon)*(this->lon[i]-lon);
			if(londiff > aux){
				londiff = aux;
				position.second=i;
			}
		}

	}

	return position;

};
	

sparseMatrix_sf::sparseMatrix_sf(node_sf ** nodes, int amount){
	double * latrange= (double*)malloc(sizeof(double)*amount);
	double * lonrange= (double*)malloc(sizeof(double)*amount);
	double prevlat;
	double prevlon;
	int i,k,j;

	for(i=0; i<amount; i++){
		latrange[i]=nodes[i]->getLat();
		lonrange[i]=nodes[i]->getLon();
	}


	quick_sort(latrange, 0, amount-1);
	quick_sort(lonrange, 0, amount-1);
	
	this->latSize=1;
	this->lonSize=1;
	
	prevlat=latrange[0];
	prevlon=lonrange[0];
	
	for(i=1; i<amount; i++){
		
		if(prevlat==latrange[i]){
			latrange[i]=-1;
		}else{
			this->latSize++;
			prevlat=latrange[i];
		}

		if(prevlon==lonrange[i]){
			lonrange[i]=-1;
		}else{
			this->lonSize++;
			prevlon=lonrange[i];
		}

	}

	

	this->lat= (double*)malloc(sizeof(double)*this->latSize);

	this->lon= (double*)malloc(sizeof(double)*this->lonSize);
	
	k=0;
	j=0;
	
	for(i=0; i<amount; i++){
		if(latrange[i]!=-1 ){
			this->lat[k]=latrange[i];
			k++;
		}
		if(lonrange[i]!=-1 ){
			this->lon[j]=lonrange[i];
			j++;
		}
	}
	
	
	free(latrange);
	free(lonrange);

	this->line = (nodeMatrix_sf**)calloc(sizeof(nodeMatrix_sf*), this->lonSize);

	this->column = (nodeMatrix_sf**)calloc(sizeof(nodeMatrix_sf*), this->latSize);
	
	
	
	for(i=0; i<amount ;i++){
		
	
		if(this->add(nodes[i])==false){
			printf("Error! I could not insert this element to sparseMatrix!\n");
		}
	}

};

nodeMatrix_sf * sparseMatrix_sf::find (int x , int y ){
	nodeMatrix_sf * aux = this->line[y];

	while(aux!=NULL){
		if(aux->getLon()==this->lon[x]){
			break;
		}else if(aux->getLon()>this->lon[x]){
			aux=NULL;
			break;
		}
		aux = aux->getEast();
	}

	return aux;
};

double sparseMatrix_sf::EuclideanDistance(double x1, double y1, double x2, double y2){
        return ((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));

}



double sparseMatrix_sf:: EsfericEuclideanDistance(double x1, double y1, double x2, double y2){
  double x1_=(pi/180)*x1;
  double x2_=(pi/180)*x2;
  double y1_=(pi/180)*y1;
  double y2_=(pi/180)*y1;

  return 6371000.00*acos(sin(x1_)*sin(x2_)+cos(x1_)*cos(x2_)*cos((y1-y2)*(pi/180)));



}

double sparseMatrix_sf::EsfericEuclideanDistance(nodeMatrix_sf * a, nodeMatrix_sf *b){
	double x1,y1,x2,y2;

	// pi is global variable from csvFileCrime_sf.h
	y1=(pi/180)*a->getLat();
	x1=(pi/180)*a->getLon();

	y2=(pi/180)*b->getLat();
	x2=(pi/180)*b->getLon();


	 double x1_=(pi/180)*x1;
     double x2_=(pi/180)*x2;
  	 double y1_=(pi/180)*y1;
 	 double y2_=(pi/180)*y1;
  
  	return 6371000.00*acos(sin(x1_)*sin(x2_)+cos(x1_)*cos(x2_)*cos((y1-y2)*(pi/180)));
  


	
	
	

};	

double sparseMatrix_sf::EsfericEuclideanDistance(double x1, double y1, nodeMatrix_sf *b){
	double x2,y2;
	// pi is global variable from csvFileCrime_sf.h
	y2=(pi/180)*b->getLat();
	x2=(pi/180)*b->getLon();


	  double x1_=(pi/180)*x1;
	  double x2_=(pi/180)*x2;
	  double y1_=(pi/180)*y1;
	  double y2_=(pi/180)*y1;
  
  	return 6371000.00*acos(sin(x1_)*sin(x2_)+cos(x1_)*cos(x2_)*cos((y1-y2)*(pi/180)));
  

	//return sqrt(pow(x1 - b->getLon(),2)+pow(y1 - b->getLat(),2));

};

void sparseMatrix_sf::collectNodes(nodeMatrix_sf * source, nodeMatrix_sf * current,const char * direction, double range, nodeMatrix_sf** list,  int * count){



};

pair <int, int> sparseMatrix_sf::ClosestValue(double latitude, double longitude){
	
	double current;
	double minorlat, minorlon;
	pair <int, int> coordinate ;
	pair <int, int> seq_coordinate;
	seq_coordinate.first=seq_coordinate.second=coordinate.first=coordinate.second=-1;
	minorlat=minorlon=10e20;
	
	BinarySearch(this->lat, latitude,  0, this->latSize-1, &minorlat, &coordinate.second);
	BinarySearch(this->lon, longitude, 0, this->lonSize-1, &minorlon, &coordinate.first);
	
	return coordinate;

};

void sparseMatrix_sf::BinarySearch(double * v , double value, int beg, int end, double *dist, int *bestfit){
	if(end<beg) return ;

	int middle = (beg+end)/2;
	
	if(v[middle]==value){
		(*dist)=0;
		(*bestfit)=middle;
		return;
	}
	else if(value<v[middle]){
		if((value-v[middle])*(value-v[middle]) < (*dist) ){
			(*dist)= (value-v[middle])*(value-v[middle]) ;
			(*bestfit)=middle;			
		}
		return BinarySearch(v, value, beg, middle-1, dist, bestfit);
	}else{
		if((value-v[middle])*(value-v[middle]) < (*dist) ){
			(*dist)= (value-v[middle])*(value-v[middle]) ;
			(*bestfit)=middle;			
		}
		return BinarySearch(v, value, middle+1, end, dist, bestfit);
	}	
	

}


int sparseMatrix_sf::BinarySearch(double * array, double value, int beg, int end){
	if(end<beg)
		return -1;
	int middle = (beg+end)/2;

	if(array[middle]==value){
		return middle;
	}else if(value<array[middle]){
		int result = BinarySearch(array, value, beg, middle-1);
		if(result==-1)
			return middle;
		else
			return result;
	}else{
		int result = BinarySearch(array, value, middle+1, end);
		if(result==-1)
			return middle;
		else
			return result;
	}


};

void sparseMatrix_sf::test(){

};

void sparseMatrix_sf::ApplyAverageOnRange(double range, csvCrimeFile_sf * header){
	
	
	int success;
	double lat, lon;
	int x,y;
	double minor, current;
	int i,k, j;
	nodeMatrix_sf ** list;
	int amount =0;
	nodeMatrix_sf * aux;
	y=x=-1;	
	i=0;
	int counter=-1;
	pair <int,int> approximation;
	int limit =1000;
	while ( !(x!=-1 && y!=-1) && i<header->getVariablesAmount()){
		
		if(strcmp(header->getVariable(i), "X")==0){
	
			x=i;
	
		}
	
		if(strcmp(header->getVariable(i),"Y")==0){
	
			y=i;
	
		}
	
		i++;
	
	}

	if(x==-1 || y==-1){
		
		printf("Error X and Y are not in this header\n");
		return;
	}

	// find the position of crime category (header)
	for(k=0; k<header->getVariablesAmount(); k++){
		if(strcmp(header->getVariable(k), "Category")==0) 
			break;
	}

	if(strcmp(header->getVariable(k), "Category")!=0){
		printf("Error: 'Category' is not a varible in header!\n");
		return;
	} 

	for(i=0; i<this->lonSize; i++){ // initialize counters  
		aux = this->line[i];
		while(aux!=NULL){
			aux->getNode()->setPossibleCrimesAmount(header->getTypeOfCrimes());
			aux->getNode()->InitializeCounters();
			aux=aux->getEast();
		}
	}	
	
		
	list=NULL;
	amount =0;
	do{	
	//	printf("\rprocessed since so ... %d", counter++);
		success = header->LoadNextTuple(); // load new crime report 

		if(success){
		
			
			lat=strtod(header->getValue(y),NULL); 
		
			lon=strtod(header->getValue(x),NULL);
			printf("\r%.11lf\t%.11lf\n", lat, lon);
		
			// binary search
		
			approximation = this->ClosestValue(lat, lon); // approximate better point in lat and lon index 
		
		
			current=0;
			minor =10e20;

			if(list!=NULL){
				free(list);
				list=NULL;
				amount =0;
			}
			j=1;
			while(amount ==0){
				if(j>1)
					printf("\nresize range!\n");
				this->generateRetangle(range*j,approximation.first, approximation.second, &list, &amount);
				j*=10;	
			}		
		
			aux = NULL;

			for(i=0; i<amount; i++){ // check all elements inside retangle of center (crimeLat, crimeLon) and range "range"

				aux = list[i];		
			
					if(list[i]!=NULL){
						list[i]->getNode()->incrementTotalCrimesCounter();
						list[i]->setNeighborsAmount(amount);
						for(j=0; j<header->getTypeOfCrimes(); j++){
							if(strstr(header->getValue(k), header->getCrimeName(j))!=NULL){
								list[i]->getNode()->incrementCrimeCounterOfClass(j);
								
							}
						}

					}
			

			}

		}	

		header->destroyTuple();
	
		if(limit ==0){
			break;
		}
	}while(success);
	if(list!=NULL) 
		free(list);	
	printf("\ndone!\n");
	
	printf("applying average....");
	
	for(i=0; i<this->lonSize; i++){
		aux = this->line[i];
		while(aux!=NULL){

			for(j=0; j<header->getTypeOfCrimes(); j++) 
				if(aux->getNode()->getCrimesOfClass(j)!=0)
					aux->getNode()->setCrimesOfClass(j,aux->getNode()->getCrimesOfClass(j)/aux->getNeighborsAmount());

			aux= aux->getEast();
		}
	}
	printf("done\n");
	




}


void sparseMatrix_sf::Classification(csvCrimeFile_sf * header, double range){
	int success;
	double lat, lon;
	int x,y;
	double minor, current;
	int i,k, j;
	nodeMatrix_sf ** list;
	int amount =0;
	nodeMatrix_sf * aux;
	y=x=-1;	
	i=0;
	while ( !(x!=-1 && y!=-1) && i<header->getVariablesAmount()){
		
		if(strcmp(header->getVariable(i), "X")==0){
	
			x=i;
	
		}
	
		if(strcmp(header->getVariable(i),"Y")==0){
	
			y=i;
	
		}
	
		i++;
	
	}

	if(x==-1 || y==-1){
		
		printf("Error X and Y are not in this header\n");
		return;
	}

	// find the position of crime category (header)
	for(k=0; k<header->getVariablesAmount(); k++){
		if(strcmp(header->getVariable(k), "Category")==0) 
			break;
	}

	if(strcmp(header->getVariable(k), "Category")!=0){
		printf("Error: 'Category' is not a varible in header!\n");
		return;
	} 
	
	for(i=0; i<this->lonSize; i++){ // initialize counters  
		aux = this->line[i];
		while(aux!=NULL){
			aux->getNode()->setPossibleCrimesAmount(header->getTypeOfCrimes());
			aux->getNode()->InitializeCounters();
			aux=aux->getEast();
		}
	}	
	
	int counter=-1;
	pair <int,int> approximation;
	int limit =1000;
	FILE *fp = fopen("sparseMatrixNodesRelation.txt", "w");
	list=NULL;
	amount =0;
	
	do{	
		//printf("\rprocessed since so ... %d", counter++);
		success = header->LoadNextTuple(); // load new crime report 

		if(success){
		
			
			lat=strtod(header->getValue(y),NULL); 
		
			lon=strtod(header->getValue(x),NULL);
			//printf("'%s, %s'\n", header->getValue(y), header->getValue(x));

			// binary search
		
			approximation = this->ClosestValue(lat, lon); // approximate better point in lat and lon index 
		//	printf("\nbetter aproximation to %.15lf,%.15lf is %.11f,%.11lf\n",lat, lon, this->lat[approximation.second], this->lon[approximation.first]);	
		
			current=0;
			minor =10e20;

			if(list!=NULL){
				free(list);
				list=NULL;
				amount =0;
			}
			j=1;
	
			while(amount ==0){
				if(j>1)
					printf("\nresize range!\n");
				//printf("generating retangle...\n");	
				this->generateRetangle(range*j,approximation.second, approximation.first, &list, &amount);
				//printf("retangle made\n");
				j*=10;	
			}		
		
			aux = NULL;

				//printf("\n%.11lf\t%.11lf\n", lat, lon);
			for(i=0; i<amount; i++){ // check all elements inside retangle of center (crimeLat, crimeLon) and range "range"

				//printf("\t%.11lf\t%.11lf\n", list[i]->getLat(), list[i]->getLon());

				
				current = EsfericEuclideanDistance(lat, lon, list[i]->getLat(), list[i]->getLon());
				 // EUCLIDEAN DISTANCEcurrent = EuclideanDistance(lat, lon, list[i]->getLat(), list[i]->getLon());
				
				//printf("\t%.11lf\n", current);
				

				
				if(current<minor){
					
					minor =current;
			
					aux = list[i];
				}else if(current==minor){
					if(aux->getLat()>list[i]->getLat()){
						minor =current;
			
						aux = list[i];
					}else if( aux->getLat()==list[i]->getLat()){
						if(aux->getLon()>list[i]->getLon()){
							minor =current;
			
							aux = list[i];
						}
					}
				}	
			
			}
			
				//printf("minor : %.11lf\t%.11lf\t%lf\n", aux->getLat(), aux->getLon(), minor);


			int flag =0;
			if(aux!=NULL){
				aux->getNode()->incrementTotalCrimesCounter();
				fprintf(fp, "%.11lf\t%.11lf\t%.11lf\t%.11lf\t%lf\t%.20lf\n", lat, lon, aux->getLat(), aux->getLon(), minor, EuclideanDistance(lat, lon, aux->getLat(), aux->getLon()));
				for(i=0; i<header->getTypeOfCrimes(); i++){
					if(strstr(header->getValue(k), header->getCrimeName(i))!=NULL){
						aux->getNode()->incrementCrimeCounterOfClass(i);
						flag =1;
					}
				}

			}
				


			if(flag==0){
				//printf("\ncrime '%s' does not fit into Crimes List\n", header->getValue(k));
			}

		}	

		header->destroyTuple();
	
		if(limit ==0){
			break;
		}
	}while(success);
	printf("\ndone!\n");
	fclose(fp);
		if(list!=NULL) free(list);	

};
bool sparseMatrix_sf::generateRetangle(double range, int x, int y, nodeMatrix_sf *** list_ref, int * amount){ 
	
	// should range be the maximum variation on absolute value of each coordinate or the top distance using esferic measure?

	/*
	
	SparseMatrix:

		[latitude --> column] .: x 
	
		[longitude--> line ]  .: y

	

	*/

	nodeMatrix_sf ** list;
	double limit;
	int xmax, xmin, ymax, ymin;
	int i;
	int count;
	nodeMatrix_sf *  aux;
	xmax=xmin=x;
	ymax=ymin=y;
	

	// establish period [ymin;ymax] and [xmin;xmax]
	while( ( (xmax<this->latSize-1) and (abs(this->lat[xmax]-this->lat[x])<=range)) or ((xmin>0) and (abs(this->lat[x]-this->lat[xmin])<=range))){
		

		if( (xmax<this->latSize-1) and (abs(this->lat[xmax]-this->lat[x])<=range))
			xmax++;


		if( (xmin>0) and (abs(this->lat[x]-this->lat[xmin])<=range) )
			xmin--;

	}
	
	//printf("\t\tretange lat: %lf-%lf=%lf\n", lat[xmax], lat[x], abs(lat[xmax]-lat[x]));
	
	//printf("\t\tretange lat: %lf-%lf=%lf\n", lat[xmin], lat[x], abs(lat[xmin]-lat[x]));

	while(((ymax<this->lonSize-1) and (abs(this->lon[ymax]-this->lon[y])<=range)) or ((ymin>0) and ((abs(this->lon[y]-this->lon[ymin]))<=range) )){
		if((ymax<this->lonSize-1) and (abs(this->lon[ymax]-this->lon[y])<=range) )
			ymax++;
		if((ymin>0) and (abs(this->lon[y]-this->lon[ymin])<=range) )
			ymin--;	
	}
	

	//printf("\t\tretange lon: %lf-%lf=%lf\n", lon[ymin], lon[y], abs(lon[y]-lon[ymin])); 
	//printf("\t\tretange lon: %lf-%lf=%lf\n", lon[ymax], lon[y], abs(lon[y]-lon[ymax])); 
	
	/*check period*/
	 //printf("SparseMatrix::generateRetangle() I :: latitude_period: [%.11lf : %.11lf] | longitude_period: [%.11lf : %.11lf]\n", lon[ymin], lon[ymax], lat[xmin], lat[xmax]);	
	
	list = NULL;
	
	count = 0;

	//range = min (max (ymax- y, y - ymin), max (xmax-x, x-xmin));	
	for(i=ymin; i<=ymax; i++){ // up->down (line), left->right(column)
	
		aux = this->line[i];
		while(aux!=NULL){
			
			
			if(aux->getLat()>=this->lat[xmin]){ 

				if(aux->getLat()<=this->lat[xmax]){ 
	
					// collect node if belong to circle of radius r 
					if(  EuclideanDistance(aux->getLat(), aux->getLon(), lat[x], lon[y]) <= (range*range) ){ 
						
						//printf("(%.15lf,%.15lf)\n",aux->getLat(), aux->getLon());
			
						list=(nodeMatrix_sf**)realloc(list, sizeof(nodeMatrix_sf*)*(count+1));
	
						list[count++]=aux;

					}
					
	
				}else{
	
					break;
	
				}
			}

			aux=aux->getEast();
		}
	}

	//printf("pos loop\n");
	
	(*amount) =count;
	(*list_ref)=list;


	if(list==NULL){
		printf("I could not found a single element inside the square of source (%lf:%lf) and range %lf\n", this->lat[x], this->lon[y], range);
		return false;
	}
	


	return true;
};



bool sparseMatrix_sf::add(node_sf* node){
	nodeMatrix_sf* aux;
	nodeMatrix_sf* caps=NULL;

	int i, j;
	
	int range;

	i=BinarySearch(this->lon, node->getLon(), 0, this->lonSize-1);
	
	j=BinarySearch(this->lat, node->getLat(), 0, this->latSize-1);
	

	if(i==-1 or j==-1){
		printf("impossible insertion! lat or lon does not exist at label\n");
		return false;
	}


	caps = new nodeMatrix_sf (node); 
		
	/* LINE INSERTION */

	aux = this->line[i];

	if (aux==NULL){ // first element at line 
		this->line[i]=caps;
	}else{ // there is an inserted element 
		if(aux->getLat()>=caps->getLat()){ // if lat(already inserted) >= lat(current), then insert before
			aux->setWest(caps);
			caps->setEast(aux);
			this->line[i]=caps; 	
		}else{ // else keep searching 
			
			// middle insertion
			while(aux->getEast()!=NULL){ // while (Exists right side) 
				if(aux->getEast()->getLat()>=caps->getLat()){// if right is greater or equal to current, then insert before
					//set current pointers 
					caps->setWest(aux); // set left 
					caps->setEast(aux->getEast()); // set right
					// update previous inserted
					aux->getEast()->setWest(caps); 
					aux->setEast(caps);
					break;
				}
				aux = aux->getEast();	// moving right 
			}
			
			if(caps->getWest()==NULL and caps->getEast()==NULL){ // end insertion 
				// while breaked due to aux->getEast() == NULL, then:
				aux->setEast(caps);
				caps->setWest(aux);
			}	

		}
	}		


	/*COLUMN INSERTION*/ 

	aux = this->column[j];

	if(aux==NULL){
		this->column[j]=caps;
	}else{
		if(aux->getLon()>=caps->getLon()){
			aux->setNorth(caps);
			caps->setSouth(aux);
			this->column[j]=caps;
		}else{
			while(aux->getSouth()!=NULL){
				if(aux->getSouth()->getLon()>=caps->getLon()){
					caps->setNorth(aux);
					caps->setSouth(aux->getSouth());
					caps->getSouth()->setNorth(caps);
					caps->setSouth(caps);
					break;
				}
				aux = aux->getSouth();
			}
			if(caps->getNorth()==NULL and caps->getSouth()==NULL){
				caps->setNorth(aux);
				aux->setSouth(caps);
			}
		}
	}
	
	
	return true;
}

sparseMatrix_sf::~sparseMatrix_sf(){
	
	nodeMatrix_sf * aux, *next;


	// delete each node
	
	for(int i=0; i<this->lonSize; i++){
		aux= this->line[i];
		while(aux!=NULL){	
			next= aux->getEast();
			delete aux;
			aux = next;
		}
	}

	// free auxiliar structures
	if(this->line!=NULL	)
		free(this->line);
	if(this->column!=NULL)
		free(this->column);
	if(this->lat!=NULL)
		free(this->lat);
	if(this->lon!=NULL)
		free(this->lon);
	
};
nodeMatrix_sf * sparseMatrix_sf::getElemAt(double lat, double lon){
	return NULL;
};
		

