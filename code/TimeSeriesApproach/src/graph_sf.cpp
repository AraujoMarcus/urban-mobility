#include "../lib/graph_sf.h"


	
graph_sf::graph_sf(node_sf ** nodes, int NodesAmount){
	this->nodes= nodes;
	this->size= NodesAmount;
	this->edgesAmount =0;
	this->list = (nodeGraph_sf **)calloc(sizeof(nodeGraph_sf*),this->size);
	this->orderList(); // sort vertices by id  
	

};




graph_sf::~graph_sf(){

	if(this->list!=NULL&& this->size>0){	
		
		nodeGraph_sf * aux, *next;
		
		for(int i=0; i<this->size; i++){
			if(this->list[i]!=NULL){
				aux =this->list[i];
				while(aux!=NULL){
					next = aux->getNext();
					delete aux;
					aux = next;
				}
			}
		}
		free(this->list);
	}
};

int graph_sf::getEdgesAmount(){
	nodeGraph_sf* aux;
	int count=0;
	for(int i=0; i<this->size; i++){
		aux =this->list[i];
		while(aux!=NULL){
			count++;
			aux= aux->getNext();
		}
	}
	return count;

};

int  graph_sf::CheckNeighborhood(double ** update, int i, int k){
	int j;
	int types = nodes[0]->getPossibleCrimesAmount();
	nodeGraph_sf  * aux = this->list[i];
	int count=0;
	
		while(aux!=NULL){ 

			count++; // amount of adj nodes 
			
			for(j=0; j<types; j++){ // sum of crimes
				
				update[k][j]+=aux->getNode()->getCrimesOfClass(j);
			}

			aux = aux->getNext();
		}
	return count;
}

void graph_sf::knn_soft(){
	double ** update = (double**)malloc(sizeof(double)*this->size);
	int i,j;
	int types = nodes[0]->getPossibleCrimesAmount();
	int count =0;
	nodeGraph_sf * aux;


	for(i=0; i<this->size; i++){
		update[i]=(double*)malloc(sizeof(double)*types);
	}

	for(i=0; i<this->size; i++){

		count=1; 
		
		for(j=0; j<types; j++){
			count+=update[i][j]= this->nodes[i]->getCrimesOfClass(j);
		}
		int position;

		count+=this->CheckNeighborhood(update, i, i); // check 1st level 
		
		aux=this->list[i];
		
		while(aux!=NULL){ // second level analysis 

			this->getNodeById(aux->getNodeId(), 0, this->size-1, &position);

			count+=this->CheckNeighborhood(update, position, i); // check 2nd level 
			
			aux=aux->getNext();
		}

		for(j=0; j<types; j++){
			update[i][j]/=count;
		}

	}
	int sum;
	for(i=0; i<this->size; i++){
		sum =0;
		printf("%lf,%lf,", this->list[i]->getNode()->getLat()
			,this->list[i]->getNode()->getLon());
		for(j=0; j<types; j++){
			sum +=update[i][j];
			printf(",%lf",update[i][j]);
			this->nodes[i]->setCrimesOfClass(j, update[i][j]);
		}
		printf("\n");
		this->nodes[i]->setTotalCrimes(sum);
		printf("\t\ttotal: %lf\n", sum);
		free(update[i]);
	}

	free(update);


}

char * graph_sf::readLine(FILE * fp){ // read an unique line from file 
	char *input =NULL;
	char  c;
	int count =0;
	do{	
		c=fgetc(fp);
		if(c!='\n'&&c!=EOF){
			input=(char*)realloc(input, sizeof(char)*(count+1));	
			input[count++]=c;
		}else if(count>0){
			input=(char*)realloc(input, sizeof(char)*(count+1));	
			input[count++]='\0';
		}
	}while(c!='\n'&&c!=EOF);


	return input;
}

void graph_sf::loadEdges(char *filepath ){

	FILE * EdgeFile = fopen(filepath,"r");
	if(EdgeFile==NULL){
		printf("Error: I could not open '%s'\n", filepath);
		return;
	}
	char * p, *q;
	char * input=NULL;
	double length, source, destiny; 
	int count =0;
	
	do {
		if(input !=NULL)
			free(input);
		input = readLine(EdgeFile);

	}while(input[0]=='#'); // ignores header
	
	fseek(EdgeFile, -(strlen(input)+1), SEEK_CUR);
	free(input);
	input=NULL;
	int result;
	
	do{

		if(input!=NULL) 
			free(input);
		
		input = readLine(EdgeFile);
		if(input!=NULL){
			
			source = strtod(input, &q);
			destiny= strtod(q, &p);
			length = strtod(p, NULL);
			
			

			
			result = this->addEdge(source, destiny,length);
				if(result==1)
					count++;	
			
			// in case of conversion digraph -> graph 	
			
			result = this->addEdge(destiny,source,length);
				if(result==1)
					count++;
		
						
		}


	}while(input!=NULL);
	
	printf("\t\tLoadEdges: %d edges inserted!\n", count);
	fclose(EdgeFile);

};


node_sf * graph_sf::getNodeById(double id, int beg, int end, int *ref){ 
	
	if(end<beg)
		return NULL;
	

	int middle = (end-beg)/2 + beg;
	
	
	
	if(this->nodes[middle]->getId() == id){
	
		*ref=middle;
	
		return this->nodes[middle];
	
	}else if(id < (this->nodes[middle]->getId()) ){
	
		return getNodeById(id, beg, middle-1, ref);
	
	}else if(id > (this->nodes[middle]->getId()) ) {
		
		return  getNodeById(id, middle+1, end, ref);
	}

};	

int graph_sf::mediana (double * candidate, int *local){

	if(candidate[0]>= candidate[0] && candidate[0]<=candidate[1]) return local[0];
	if(candidate[1]>= candidate[0] && candidate[0]<=candidate[0]) return local[0];

	if(candidate[0]>= candidate[1] && candidate[1]<=candidate[2]) return local[1];
	if(candidate[2]>= candidate[1] && candidate[1]<=candidate[0]) return local[1];

	if(candidate[0]>= candidate[2] && candidate[2]<=candidate[1]) return local[2];
	if(candidate[1]>= candidate[2] && candidate[2]<=candidate[0]) return local[2];


};	

void graph_sf::Myquick_sort(node_sf ** v, int beg, int end){



	if(end-beg<=0) return ;

	int middle = (end+beg)/2;
	
	double  candidates[3] = {v[beg]->getId() ,v[middle]->getId() , v[end]->getId()};
	
	int local[3] = {beg, middle, end};
	
	int local_pivot = mediana (candidates, local);

	int i ,j;
	
	node_sf * pivot; 

	pivot = v[local_pivot];

	v[local_pivot]=v[end];

	v[end]=pivot;

	
	i=beg-1;
	j=end;

		
	do{
		do{i++;}while(v[i]->getId()<pivot->getId());
		do{j--;}while(j>=beg && v[j]->getId()>=pivot->getId());
		
		if(i<j){

			node_sf * aux;
	
			aux = v[i];
			
			v[i]=v[j];

			v[j]=aux;
	

		}
	
	}while(i<j);

	v[end]=v[i];

	v[i]=pivot;
	


	Myquick_sort(v, beg, i-1);

	Myquick_sort(v, i+1, end);

};

void graph_sf::orderList(){
	this->Myquick_sort(this->nodes, 0, this->size-1);
};



int graph_sf::addEdge(double source, double destiny, double length){
	node_sf * source_list=NULL;
	node_sf * destiny_list=NULL;
	nodeGraph_sf* aux=NULL;
	int flag=0, i;
	int s,d;

	
	source_list= this->getNodeById(source, 0, this->size, &s);
	destiny_list= this->getNodeById(destiny,0, this->size, &d);


	flag =0;
	if(source_list==NULL){
		printf("Error : I could not find vertice %lf\n", source);
		flag ++;
	}	

	if(destiny_list==NULL){
		printf("Error: I could not find vertice %lf\n", destiny);
		flag++;
	}
	
	if(flag !=0){
		return -1;
	}

		
	
	nodeGraph_sf * elem = new nodeGraph_sf (destiny_list);	
	elem ->setLength(length);
	

	aux = this->list[s];
	flag =0;
	if(aux == NULL){ // first insertion 
		this->list[s]=elem;
		elem->setPrev(NULL);
		elem->setNext(NULL);
		this->edgesAmount++;
		return 1;
	}else{



		if(aux->getNodeId() > elem->getNodeId()){ // new first element
			
			aux->setPrev(elem);
			this->list[s]=elem;
			elem->setPrev(NULL);
			elem->setNext(aux);
			this->edgesAmount++;
		
			return 1;
		
		}else{

			if(aux->getNodeId() == elem->getNodeId()){
 				delete elem;
 				return 0;
 			}

			if(aux->getNext()==NULL){ // end insertion (aux>elem && aux->next==NULL)
			
				aux->setNext(elem);
				elem->setPrev(aux);
				elem->setNext(NULL);
				this->edgesAmount++;
				return 1;
			}

			while(aux->getNext()!=NULL){ // betweeen insertion
 				if(aux->getNext()->getNodeId() > elem->getNodeId()){
 					
 					if(aux->getNodeId() == elem->getNodeId()){
 						delete elem;
 						return 0;
 					}

					aux->getNext()->setPrev(elem);
					elem->setNext(aux->getNext());
					aux->setNext(elem);
					elem->setPrev(aux);
					flag=1;
					this->edgesAmount++;
			
					return 1;
				}
				aux = aux->getNext();
			}

			if(aux->getNext()==NULL ){ // end insertion
				aux->setNext(elem);
				elem->setPrev(aux);
				elem->setNext(NULL);
				this->edgesAmount++;
			
				return 1;
			}
			
		}

		
		return -2;
	}

	

};


void graph_sf::report(char * filepath){
	FILE * fp = fopen(filepath, "w");
	if(fp==NULL){
		printf("\t\tError: I could not open '%s', so sorry =(\n", filepath);
		return;
	}

	nodeGraph_sf * aux=NULL;
	int * TS;
	int length;
	fprintf(fp, "id, Weigth, neighborn_id + Edge_length\n");
	fprintf(fp, "#%d %d 11\n", this->size, this->edgesAmount/2);
	
	for(int i=0; i<this->size; i++){
		aux = this->list[i];
		fprintf(fp, "%.0lf,%.0lf", this->nodes[i]->getId(),this->nodes[i]->getTotalCrimes());
		while(aux!=NULL){
			fprintf(fp ,",%.0lf,%.0lf", aux->getNodeId(), aux->getLength());		
			aux = aux->getNext();
		}	
		fprintf(fp,"\n");
	}
	fclose(fp);

};



