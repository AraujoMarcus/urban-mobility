#include "../lib/nodeMatrix_sf.h"


using namespace std;

nodeMatrix_sf::nodeMatrix_sf(){

};

nodeMatrix_sf::nodeMatrix_sf(node_sf * node){
	if(node==NULL){
		printf("fatal error! null node given to sparse matrix! I can't create an element at matrix with it!\n");
		exit(0);
	}
	this->node= node;
	this->n = this->s = this->w = this->e=NULL;


};
nodeMatrix_sf::~nodeMatrix_sf(){

};
node_sf * nodeMatrix_sf::getNode(){
	return this->node;
};
double nodeMatrix_sf::getNodeId(){
	return this->node->getId();
};
double nodeMatrix_sf::getLat(){
	return this->node->getLat();
};
double nodeMatrix_sf::getLon(){
	return this->node->getLon();
};

nodeMatrix_sf* nodeMatrix_sf::getNorth(){
	return this->n;
};

nodeMatrix_sf* nodeMatrix_sf::getSouth(){
	return this->s;
};
nodeMatrix_sf* nodeMatrix_sf::getWest(){
	return this->w;
};
nodeMatrix_sf* nodeMatrix_sf::getEast(){
	return this->e;
};
nodeMatrix_sf** nodeMatrix_sf::getNorthEast(int range){
	return NULL;
};
nodeMatrix_sf** nodeMatrix_sf::getNorthWest(int range){
	return NULL;
};
nodeMatrix_sf** nodeMatrix_sf::getSouthWest(int range){
	return NULL;
};
nodeMatrix_sf** nodeMatrix_sf::getSouthEast(int range){
	return NULL;
};
void nodeMatrix_sf::setNeighborsAmount(int value){
	this->neighbors=value;
};
void nodeMatrix_sf::setWest(nodeMatrix_sf * w){
	this->w=w;
};
void nodeMatrix_sf::setEast(nodeMatrix_sf * e){
	this->e=e;
};
void nodeMatrix_sf::setNorth(nodeMatrix_sf * n){
	this->n=n;
};
void nodeMatrix_sf::setSouth(nodeMatrix_sf * s){
	this->s=s;
};
int nodeMatrix_sf::getNeighborsAmount(){
	return this->neighbors;
};
