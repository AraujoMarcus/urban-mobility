// system libraries
#include <iostream>
#include <math.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string.h>

// project libraries 
#include "../lib/sparseMatrix_sf.h"
#include "../lib/csvCrimeFile_sf.h"
#include "../lib/node_sf.h"
#include "../lib/graph_sf.h"

// arbirtrary choice to days in month/semester/year, so my discrete function can calculate granularity correspondence 
#define MONTH 31 
#define SEMESTER 186
#define YEAR 372	



using namespace std;

/*output : 2 files -> sorted nodes with absolute and normalized counters*/
void report(csvCrimeFile_sf * header , node_sf ** nodes, int nodesAmount); 

/*read a line, without length restriction*/
char * readLine(FILE * fp); 

/*load nodes from file*/
node_sf ** readNodes(FILE * fp, int * size, int TimeSeriesLength);

/*delete nodes list*/
void destroyNodesList(node_sf ** nodes, int size);

/*knn, k=1*/
void knn (csvCrimeFile_sf * header, node_sf ** nodes, int nodesAmount, 
		int TSgranularity, int begYear);

/*Quick sort function : to double and int*/
int mediana (double * candidate, int *local);
int mediana (int * candidate, int *local);
void Myquick_sort(node_sf ** v, int beg, int end);
void quick_sort(int * v, int beg, int end);

/*given a radius, incremment all counters inside*/
void knn_range(node_sf ** nodes, int amount, double range);

/*Time Series Length given period [Beg, end] in years and granularity */
int CalculateTSLength(int granularity, int beg, int end);

/*define granularity in amount of days */
int defineGranularity(char * option);

/*date transformation into number of days*/
int DateTransformation(char *date);

// argv { list of pertinent crimes, Crimes, Nodes, Edges, OutputFile, Approach, granularity }
int main(int argc, char * argv[]){
	if( argc < 10) {
		printf("Error: argv {List of pertinent crimes file, Crimes File, Nodes, Edges, OutputFile, Approach, granularity, TS(initial) date, TS(final) date} \n");
		return 1;
	}	
	char * option = argv[6];	
	
	
	if(!(strcmp(option, "SM") ==0 or strcmp(option, "GRAPH")==0)){
		printf("invalid option, try GRAPH or SM\n");
		return 1; 
	}

	
	clock_t t1, t2;

	FILE * CrimesFile = fopen(argv[2], "r");
	
	FILE * NodesFile = fopen(argv[3], "r");
	
	if(NodesFile == NULL || CrimesFile ==NULL){
		
		if(CrimesFile ==NULL)	
			printf("I could not open the file '%s' \n", argv[2]);
		
		if(NodesFile ==NULL)	
			printf("I could not open the file '%s' \n", argv[3]);
		

		if(NodesFile!=NULL) 
	
			fclose(NodesFile);
	
		if(CrimesFile!=NULL) 
	
			fclose(CrimesFile);
	
		return 1;
	}

	/*structures*/
	node_sf ** nodes =NULL; // keep in memory graph vertices 
	graph_sf * graph =NULL;
	int NodesAmount; 


	/*temporal parameters*/
	int TSgranularity = defineGranularity(argv[7]);
	
	int initialYear= DateTransformation(argv[8]);
	
	int endingYear = DateTransformation(argv[9]);
	
	int TSlength = CalculateTSLength(TSgranularity, initialYear, endingYear);

	printf("'%s'--> %d\n'%s'--> %d\nTS length == %d\n",argv[8], initialYear, argv[9], endingYear,  TSlength);
	

	//execution time 
	int hour, min;
	double sec;
	csvCrimeFile_sf * header = NULL; // keep in memory the crime file header.
	sparseMatrix_sf * matrix = NULL;
	bool report_or_not =true;

	t1=clock();

	/*preprocessing*/

	
	printf(" reading nodes from '%s'...\n", argv[2]);	
	nodes = readNodes(NodesFile, &NodesAmount, TSlength); // read data graph vertices
	

	printf("nodesAmount: %d\n",NodesAmount);
	t2=clock();
	sec = (t2-t1)/CLOCKS_PER_SEC;
	hour = sec/3600;
	min = (sec - hour*3600)/60; 
	sec = sec - 60*min - 3600*hour;
	printf("\tPreprocessing done. run time : %d hrs %d min %lf sec\n", hour, min, sec);



	/*analysis*/
	

	t1=clock();

	if(strcmp(option, "GRAPH")==0){
		
		
		header =  new csvCrimeFile_sf(CrimesFile, argv[1]); // what kind of data are going to deal with ?
		header->printHeader(); 
		printf("running knn...\n");
		knn ( header,  nodes,  NodesAmount, TSgranularity, initialYear);
		printf("done!\n");
		printf("building graph...\n");
		graph= new graph_sf(nodes, NodesAmount);
		printf("structure built, loading edges...\n");
		graph->loadEdges(argv[4]);
		printf("edges loaded!\n");
		printf("\t\tEdgesCount:%d edges counted!\n", graph->getEdgesAmount());
		graph->report(argv[5]);
		
			
	}else if(strcmp(option,"SM")==0){
		if(argc<8){
			printf("Sparse Matrix approach requires range\n");
			
		}else{
			printf("creating sparse Matrix...\n");
               		matrix = new sparseMatrix_sf (nodes, NodesAmount);
                	header =  new csvCrimeFile_sf(CrimesFile, argv[1]);
               		printf("range : %lf\n", atof(argv[7])); 
			matrix->Classification(header, TSlength);
                	printf("\rsparse Matrix created\t\n");
		}

	}
	
	Myquick_sort(nodes, 0, NodesAmount -1);
	report(header, nodes, NodesAmount);
	
	t2=clock();
	sec = (t2-t1)/CLOCKS_PER_SEC;
	hour = sec/3600;
	min = (sec - hour*3600)/60; 
	sec = sec - 60*min - 3600*hour;
	printf("\tAnalysis done. run time : %d hrs %d min %lf sec\n", hour, min, sec);



	/*free memory*/
	if(header!=NULL) 
		delete header;
	if(graph!=NULL)
		delete graph;
	if(matrix!=NULL)
		delete matrix;
	destroyNodesList(nodes, NodesAmount);
	fclose(NodesFile);
	fclose(CrimesFile);

	return 0;
}	

int DateTransformation(char *date){
/*
         *      date format : mm/dd/yyyy
         * */

        int year, month, day, position;

        year = month = day =0;


        /*Split policy*/

        char * p = date;

        while(*p!='/'){
		month*=10; // * here 
                month+=((int)*p)-48;
                p++;
        }

        p++;

        while(*p!='/'){
		day *=10; // *here 
                day+=((int)*p)-48;
                p++;
        }

        p++;

        while(*p!='\0'){
		year*=10; // *here
                year+=((int)*p) -48;
                p++;
        }

        return year*372 + month *31 + day;

};

int defineGranularity(char *option){

	if(strcmp(option, "MONTH")==0)
		return MONTH;
	else if(strcmp(option, "SEMESTER")==0)
		return SEMESTER;
	else if (strcmp(option, "YEAR")==0)	
		return YEAR;
	else		
		exit(0);
			
	
	

}

int CalculateTSLength(int granularity, int beg, int end){
	/* (end - beg = periodLength) , then : periodLength/granularity = vectorSize  */
	int length =(((end) - beg)/granularity )+ 1;
	
	
	return length;

}

double EsfericEuclideanDistance(double x1, double y1, double x2, double y2){
  double x1_=(pi/180)*x1;
  double x2_=(pi/180)*x2;
  double y1_=(pi/180)*y1;
  double y2_=(pi/180)*y1;
  
  return 6371000.00*acos(sin(x1_)*sin(x2_)+cos(x1_)*cos(x2_)*cos((y1-y2)*(pi/180)));
  


}

double EuclideanDistance(double x1, double y1, double x2, double y2){
	return ((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));

}


void knn_range(node_sf ** nodes, int amount, double range){
	long int rrn ;
	double id, distance;
	double ** update=NULL;
	FILE * in = fopen("distanceVertices.bin", "r");
	int i ,j, k;
	int types=nodes[0]->getPossibleCrimesAmount();



	/*We can't just update each counter, cus it could cause a 2nd level update (incremmenting a node outside of the range)*/
	update = (double**)malloc(sizeof(double*)*amount);
	for(i=0; i<amount; i++)
		update[i]=(double*)calloc(sizeof(double), types);



	for(i=0 ; i<amount ; i++){ // for each node find elements inside the range 
		
		rrn = i*(sizeof(double)*2*(amount-1)); // why 2 ? (distance+id)
	
		fseek(in, rrn, SEEK_SET);
		
		int counter=0;
		do{

				

			/*file  format : "id distance" */
			fread(&id, sizeof(double), 1, in);
			fread(&distance, sizeof(double), 1, in);
			
			if(distance<=range*range){ // sqrt is an expensive operation
				counter ++;
				for(j=0; j<amount; j++){
					if(nodes[j]->getId() == id){
						for(k=0; k<types; k++){
							if(nodes[i]->getCrimesOfClass(k)!=0)
								update[j][k]++;
						}
						break;
					}
				}
			}

		}while(distance<=range*range);
	
		
	}
	if(update!=NULL){
		for(k=0; k<amount ; k++){
			for(i=0; i<types; i++){
				if(update[k][i]!=0){
					
					nodes[k]->incrementCrimeCounterOfClass(i);
					nodes[k]->incrementTotalCrimesCounter();
				}
			}
			free(update[k]);
		}
		free(update);
	}
}	

bool myfunction (pair < double, double >i, pair < double, double > j){ return (i.first<j.first);}


char * readLine(FILE * fp){ // read an unique line from file 
	char *input =NULL;
	char  c;
	int count =0;
	do{	
		c=fgetc(fp);
		if(c!='\n'&&c!=EOF){
			input=(char*)realloc(input, sizeof(char)*(count+1));	
			input[count++]=c;
		}else if(count>0){
			input=(char*)realloc(input, sizeof(char)*(count+1));	
			input[count++]='\0';
		}
	}while(c!='\n'&&c!=EOF);


	return input;
}

node_sf ** readNodes(FILE * fp, int * size, int TimeSeriesLength){
	node_sf ** nodes =NULL;
	
	int count =0;
	
	char * line =NULL;
	
	char * p, *q;

	double lat, lon, id;

	int i;
	
	
	
	do{ 
		/*
			step 1 : catch a line (tuple) from file
			step 2 : extract values (lat, lon and Id) from string  
		*/


		line = readLine(fp); 
		
		if(line!=NULL){
			if(line[0]!='#'){ // ignores header 
				nodes=(node_sf**)realloc(nodes, sizeof(	node_sf*)*(count+1));
				p=line;
				i=0;
				id = strtod(line, NULL);		
				while(line[i]!='\t' and line[i]!=' ') i++;
				q=&line[i];
				lat=strtod(q, &p);
				lon=strtod(p, &q);
			nodes[count] = new node_sf(id, lat, lon, 0, NULL, TimeSeriesLength);
				count ++;	

			}
			free(line);
		}

	}while(line!=NULL);

	*size=count;
	
	return nodes;
}

void destroyNodesList(node_sf ** nodes, int size){
	if(nodes!=NULL){
		for(int i=0; i<size; i++){
			if(nodes[i]!=NULL)
				delete nodes[i];
		}
		free(nodes);
	}	


}




void knn(csvCrimeFile_sf * header, node_sf ** nodes, int nodesAmount, int TSgranularity, int begTS ){
	int x=-1;
	int y=-1;
	int z=-1;

	int i=0;
	int k;
	int j;
	int r;
	double current=0;
	double minor =1000;
	int counter=0;

        int useful=0;
	int success;

	time_t rawtime;
    	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	printf ( "Current local time and date: %s", asctime (timeinfo) );
	printf( "knn :: I'm creating a file called 'Knn_Nodes_Crimes_result.txt' containing all assignments\n");	
	FILE * fp = fopen("Knn_Nodes_Crimes_results.txt", "w");

	

	while (!(x!=-1 && y!=-1 && z!=-1) && i<header->getVariablesAmount()){
		
		if(strcmp(header->getVariable(i), "X")==0){
	
			x=i;
	
		}else if(strcmp(header->getVariable(i),"Y")==0){
	
			y=i;
	
		}else if (strcmp(header->getVariable(i), "Date")==0){
			
			z=i;	
		
		}else if (strcmp(header->getVariable(i), "Category")==0){

			k=i;

		}
		
	
		i++;
	
	}

	if(x==-1 || y==-1 || k==-1 || z==-1){
		printf("Error: Missing data on header (X, Y, Date or Category)\n");
		exit(0);
	}



	for(i=0; i<nodesAmount; i++){
		nodes[i]->setPossibleCrimesAmount(header->getTypeOfCrimes());
		nodes[i]->InitializeCounters();
	}

 	success = header->LoadNextTuple();
	
	
	do{	
			
		
		//system("clear");	
		printf("\r");
		printf("processed since so ... %d", counter++);
		
		if(success){
			
			double lon = strtod(header->getValue(x), NULL);

			double lat = strtod(header->getValue(y), NULL);

			char * date = header->getValue(z);
			
			fprintf(fp, "%.11lf\t%.11lf\t", lat, lon); // current node position

			current =0 ;
			minor =10e20;
 
			for(i=0; i<nodesAmount; i++){

				
				
				current = EsfericEuclideanDistance(lat, lon, nodes[i]->getLat(), nodes[i]->getLon());
				
				
				if(current<minor){

					minor = current;

					j=i;

				}else if(current==minor){
					if(nodes[i]->getLat()==nodes[j]->getLat()){
						if(nodes[i]->getLon()>nodes[j]->getLon()){
							minor = current;

							j=i;
						}
					}else if(nodes[i]->getLat()>nodes[j]->getLat()){
							minor = current;

							j=i;
						
					}
				}

			}	
			
			fprintf(fp, "%.11lf\t%.11lf\t%lf\t%.20lf\n", nodes[j]->getLat(), nodes[j]->getLon(), minor, EuclideanDistance(lat, lon, nodes[j]->getLat(), nodes[j]->getLon())); // nearest element 

			int flag=0;
			for(r=0; r<header->getTypeOfCrimes(); r++){ // how many crimes ? update each counter
				if(strstr(header->getValue(k), header->getCrimeName(r))!=NULL){
					nodes[j]->incrementCrimeCounterOfClass(r);
					flag=1;
				}
			}
			if(flag==0){
				printf("crime '%s' does not fit into Crimes List\n", header->getValue(k));
			}else{
				useful++;
				nodes[j]->incrementTotalCrimesCounter(); // absolute amount of crimes located at node j (KNN)
				nodes[j]->AddToTimeSeries(date, TSgranularity, begTS);
			}
			
			header->destroyTuple();
			success = header->LoadNextTuple();	
		}

	}while(success);
	fclose(fp);
	printf("\n");
	printf("Useful info : %d\n", useful);

}

void quick_sort(int * v, int beg, int end){
	if(end-beg<=0) return ;

	int middle = (end+beg)/2;
	int candidates[3] = {v[beg] ,v[middle], v[end]};
	int local[3] = {beg, middle, end};
	int local_pivot = mediana (candidates, local);
	int pivot = v[local_pivot];
	int i ,j;

	v[local_pivot]=v[end];
	v[end]=pivot;
	i=beg-1;
	j=end;
	
	do{
		do{i++;}while(v[i]<pivot);
		do{j--;}while(v[j]>=pivot && j>=beg);
		if(i<j){
			int aux = v[i];
			v[i]=v[j];
			v[j]=aux;

		}
	
	}while(i<j);	
	v[end]=v[i];
	v[i]=pivot;
	
	quick_sort(v, beg, i-1);
	quick_sort(v, i+1, end);

		

}

void report(csvCrimeFile_sf * header , node_sf ** nodes, int nodesAmount){

	if(header==NULL || nodes == NULL ){
		printf("Error : NULL POINTER !\n");
		exit(0);
	}

 
	system("rm -r resultsCSV/; mkdir resultsCSV");
	printf("creating directory called 'resultsCSV' and savig all crimes into its respective files\n");
 	 FILE * fp_timeseries = fopen("TimeSeries.csv", "w");
	 FILE * out = fopen("outSorted.csv", "w");
	 FILE ** crimefile = (FILE**)malloc(sizeof(FILE*)*header->getTypeOfCrimes());
	 char * filename;
	 int i, k, length;
	 double * min, *max, *aux;
	 char * crime=NULL;
	 int typeofcrimes= header->getTypeOfCrimes();
	 char path [] ="./resultsCSV/";



	 	//1) absolute values 
	 for( i=0; i<typeofcrimes; i++){
	 	crime = header->getCrimeName(i);
	 	length=strlen(crime);
	 	for(k=0; k<length; k++){
	 		if(crime[k]==',' or crime[k]==' ' or crime[k]=='/')
	 			crime[k]='_';
	 	}

	
	 	filename = (char*)calloc(sizeof(char),(length+20));
	 	filename[0]='\0';
	 	strcat(filename, path);
	 	strcat(filename, crime);
	 	strcat(filename, ".csv");
	 	
	 	crimefile[i]=fopen(filename, "w");
	 	if(crimefile[i]==NULL){
	 		printf("I could not open the file '%s'\n", filename);
	 	}
	 	free(filename);
	 
	 }


	 printf("sorting nodes list...\n");
	 Myquick_sort(nodes, 0, nodesAmount-1);
	 printf("sorted!\n");

	 fprintf(out, "nodeID,lat,lon,CrimesAmount");
	 fprintf(fp_timeseries, "nodeID,lat,lon,TimeSeries\n");	 
	 for(i=0; i<typeofcrimes; i++){
	 	fprintf(out, ",%s", header->getCrimeName(i));
	 	fprintf(crimefile[i],"nodeID, lat, lon, amount\n");
	 }
	 fprintf(out, ",TimeSeries");
	 fprintf(out, "\n");

	 int * TS;
	 int tsLength;
		
	 for(i=0; i<nodesAmount; i++){
	 	fprintf(out, "%.1lf,%.10lf,%.10lf,%.10lf", nodes[i]->getId(), nodes[i]->getLat(), nodes[i]->getLon(), nodes[i]->getTotalCrimes());
	 	fprintf(fp_timeseries, "%.1lf,%.10lf,%.10lf", nodes[i]->getId(), nodes[i]->getLat(), nodes[i]->getLon());

		for(k=0; k<typeofcrimes; k++){
	 		fprintf(out, ",%.10lf", nodes[i]->getCrimesOfClass(k));
	 		fprintf(crimefile[k], "%.1lf,%.10lf,%.10lf,%.10lf\n",nodes[i]->getId(), nodes[i]->getLat(), nodes[i]->getLon(), nodes[i]->getCrimesOfClass(k));

	 	}

	 	
		tsLength = nodes[i]->getTimeSeriesLength();
		TS = nodes[i]->getTimeSeries();

		for(k=0; k<tsLength; k++){


		        fprintf(out, ",%d", TS[k]);
		        fprintf(fp_timeseries, ",%d", TS[k]);

		}
	
	 	fprintf(out, "\n");
		fprintf(fp_timeseries, "\n");
	 	
	 }

	 for(i=0; i<typeofcrimes; i++)
	 	fclose(crimefile[i]);
	 
	 fclose(out);



	 // 2)  normalized values 
	printf("saving output file with normalized values\n");
		out = fopen("outSortedNormal.csv", "w");
		 fprintf(out, "nodeID,lat,lon,CrimesAmount");
	 	if(out==NULL){
	 		printf("Sorry, I could not open the file outSortedNormal.csv\n");
	 		exit(0);
	 	}

	  for( i=0; i<typeofcrimes; i++){
	 	crime = header->getCrimeName(i);
	 	length=strlen(crime);
	 	filename = (char*)calloc(sizeof(char),(length+28)); 
	 	strcpy(filename, path);
	 	strcat(filename, crime);
	 	strcat(filename, "_Normal.csv");
	 	crimefile[i]=fopen(filename, "w");
	 	if(crimefile[i]==NULL){
	 		printf("I could not open the file '%s'\n", filename);
	 		
	 	}else{
	 		fprintf(crimefile[i], "nodeID,lat,lon,CrimesAmount");
	 	}
	 	free(filename);
	 	
	 }


	 min =(double*)calloc(sizeof(double), typeofcrimes+1); 
	 max =(double*)calloc(sizeof(double), typeofcrimes+1);
	 aux =(double*)calloc(sizeof(double), typeofcrimes+1);

	 for(i=0; i<typeofcrimes+1; i++){
	 	min[i] = 10e20;
	 }

	 printf("checking period...\n");

	 for(i=0 ; i<nodesAmount;i++){
	 	
	 	if(nodes[i]->getTotalCrimes() > max[typeofcrimes])
	 		max[typeofcrimes]=nodes[i]->getTotalCrimes();
	 	
	 	if(nodes[i]->getTotalCrimes() < min[typeofcrimes])
	 		min[typeofcrimes]=nodes[i]->getTotalCrimes();
	 	

	 	for(k=0; k<typeofcrimes; k++){
	 		if(nodes[i]->getCrimesOfClass(k)> max[k])
		 		max[k]=nodes[i]->getCrimesOfClass(k);
		 	if(nodes[i]->getCrimesOfClass(k) < min[k])
		 		min[k]=nodes[i]->getCrimesOfClass(k);
	 	}

	 }
	 printf("done!\n");

	 for(i=0 ; i<nodesAmount;i++){

	 	aux[typeofcrimes] = ((double)nodes[i]->getTotalCrimes())-min[typeofcrimes];
	 	aux[typeofcrimes] /= (max[typeofcrimes]-min[typeofcrimes]);

	 	fprintf(out, "%.1lf,%.10lf,%.10lf,%.11lf", nodes[i]->getId(), nodes[i]->getLat(), nodes[i]->getLon(), aux[typeofcrimes]);
	 	for(k=0; k<typeofcrimes; k++){
	 		aux[k] = ((double)nodes[i]->getCrimesOfClass(k)) - min[k];
	 		aux[k] /= max[k]-min[k];

	 		fprintf(out, ",%.11lf", aux[k]);
	 		fprintf(crimefile[k], "%.1lf,%.10lf,%.10lf,%.11lf\n",nodes[i]->getLon(), nodes[i]->getLat(), nodes[i]->getLon(), aux[k]);

	 	}
	 	fprintf(out, "\n");
	 
	 }
	 


		 free(min);
		 free(max);
		 free(aux);	

		for(i=0; i<typeofcrimes; i++ )
		 	fclose(crimefile[i]);
		 free(crimefile);
		fclose(out);

}


int mediana (double * candidate, int *local){

	if(candidate[0]>= candidate[0] && candidate[0]<=candidate[1]) return local[0];
	if(candidate[1]>= candidate[0] && candidate[0]<=candidate[0]) return local[0];

	if(candidate[0]>= candidate[1] && candidate[1]<=candidate[2]) return local[1];
	if(candidate[2]>= candidate[1] && candidate[1]<=candidate[0]) return local[1];

	if(candidate[0]>= candidate[2] && candidate[2]<=candidate[1]) return local[2];
	if(candidate[1]>= candidate[2] && candidate[2]<=candidate[0]) return local[2];


}	

int mediana (int * candidate, int *local){

	if(candidate[0]>= candidate[0] && candidate[0]<=candidate[1]) return local[0];
	if(candidate[1]>= candidate[0] && candidate[0]<=candidate[0]) return local[0];

	if(candidate[0]>= candidate[1] && candidate[1]<=candidate[2]) return local[1];
	if(candidate[2]>= candidate[1] && candidate[1]<=candidate[0]) return local[1];

	if(candidate[0]>= candidate[2] && candidate[2]<=candidate[1]) return local[2];
	if(candidate[1]>= candidate[2] && candidate[2]<=candidate[0]) return local[2];


}	


void Myquick_sort(node_sf ** v, int beg, int end){



	if(end-beg<=0) return ;

	int middle = (end+beg)/2;
	
	double  candidates[3] = {v[beg]->getTotalCrimes() ,v[middle]->getTotalCrimes() , v[end]->getTotalCrimes()};
	
	int local[3] = {beg, middle, end};
	
	int local_pivot = mediana (candidates, local);

	int i ,j;
	
	node_sf * pivot; 

	pivot = v[local_pivot];

	v[local_pivot]=v[end];

	v[end]=pivot;

	
	i=beg-1;
	j=end;

		
	do{
		do{i++;}while(v[i]->getTotalCrimes()<pivot->getTotalCrimes());
		do{j--;}while(j>=beg && v[j]->getTotalCrimes()>=pivot->getTotalCrimes());
		
		if(i<j){

			node_sf * aux;
	
			aux = v[i];
			
			v[i]=v[j];

			v[j]=aux;
	

		}
	
	}while(i<j);

	v[end]=v[i];

	v[i]=pivot;
	


	Myquick_sort(v, beg, i-1);

	Myquick_sort(v, i+1, end);

}

