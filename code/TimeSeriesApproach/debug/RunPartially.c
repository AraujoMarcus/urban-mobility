#include<stdio.h>
#include<stdlib.h>
#include <unistd.h>
 #include <errno.h>


int main (int argc, char * argv[]){

	if(argc<4){
		printf("enter with limit, path and range\n");
		return 1;
	}

	int limit = atoi (argv[1]);
	int counter=1;
	char path [200];
	char current[300];
	sprintf(path, "%s", argv[2]);	
	char arg [300];
	system (" mkdir OUTPUT");
	while(counter<=limit){
		
		sprintf(current, "%spart%d.csv", path, counter	);
		
		sprintf(arg, "./main %s san-francisco_noisy-vertices.txt san-francisco_noisy-edges.txt outputfile.csv SM %s > ./OUTPUT/%d.csv\n", current, argv[3], counter);		
		
		//printf("command: '%s'\n", arg);	
		
		printf("trying %d ...", counter);
		if(system(arg) == -1){
			printf("part %d presented some error\n", counter);
		}else{
			printf("part %d ran as expected\n", counter);
		}
		counter++;
	}		

	return 0;	

}


