function m = vector_to_square_matrix(x)
    
    [n_lines, ~] = size(x) ;
    
    
    m = []; 
    
    for i = 1 : (n_lines)
    
        m = [m, x*2];
      
    endfor 

endfunction
  