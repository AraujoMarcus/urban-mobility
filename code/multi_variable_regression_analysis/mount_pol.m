function pol = mount_pol (x,y,degree)
 
  [n_lines, n_var] = size(x); 
 
  pol = ones(n_lines, sum(degree(:,1)) +1);

  beg_inc =0;
  
  for j = 1:n_var
  
  
    for k =2:(degree(j)+1) 
        
        pol(:,k+beg_inc) = x(:,j).^(k-1);
             
    endfor 
    
    beg_inc = beg_inc + degree(j) +1;
      
  endfor  
 
   


endfunction 