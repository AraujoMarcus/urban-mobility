function [alphas, pol] = mmq ( x,y, dim )

[m, ~] = size (x);

pol = mount_pol(x,y,dim);

[q,r] = decomp( pol );
 
alphas = (q*r) \ y;
 
endfunction

