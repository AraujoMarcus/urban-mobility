#Author: AraujoMarcus 

import itertools 
import hashlib


def combine(E):

    r = E['amountOfElem']
    
    positions = E['positions']

    if(r > len(positions)):
    
        print("Error: invalid combination")
        
        return -1
   
    else:
        
        result = itertools.combinations(positions, r)
    
        return list(result)
            
    
def collect(record, A_comb, B_comb):

    str_a = ""
    str_b = ""

    for i in range(0, len(record)):
        
        if( i in A_comb):
            str_a = str_a + record[i]

        if( i in B_comb):
            str_b = str_b + record[i]

    print("A:"+str_a+" | B:"+str_b)

    h = hashlib.sha1()

    h.update(str_a)

    a_hash = h.hexdigest()

    h.update(str_b)

    b_hash = h.hexdigest()

    return [a_hash, b_hash]


def write_result (record, A_comb, B_comb, conf, sup):
    
    str_a = "{" 
    str_b = "{"

    for i in range (0, len(record)):

        if( i in A_comb): 
            str_a = str_a + record[i] + ","

        if( i in B_comb):
            str_b = str_b + record[i] + ","

    str_a = str_a + "}"
    str_b = str_b + "}"

    print(str_a + " --> " + str_b + ", such that: conf = " + str(conf) + ", sup = " + str(sup)) 

    

# find A -> B from dataset, with support sup and confidence conf  
def a_priori(dataset, A, B, sup, conf):
    #dataset = 2d array 
    #A, B = {'ammountOfElem' : int( ), 'positions' : [...] }
    #sup, conf belongs to interval [0,1]
        
    
    # COMB. ANALYSIS, PICK i FROM j ELEMENTS --> j! / i! (amount of combinations)
    Acombinations = combine(A)
    Bcombinations = combine(B)

    already_seen = []

    for A_comb in Acombinations:

        for B_comb in Bcombinations:

            for i in range (0, len(dataset)):


                occurrence = 0.0
                repetition = 0.0

                [a_hash , b_hash] = collect(dataset[i], A_comb, B_comb)
            
                my_hash = a_hash+":"+b_hash
        
                if(my_hash not in already_seen):
                    
                    for j in range (i, len(dataset)):
                        
                        [al_hash,bl_hash] = collect (dataset[j], A_comb, B_comb)
                            
                        if(a_hash == al_hash):
                             
                            occurrence = occurrence + 1.0
                            
                            if(b_hash == bl_hash):

                                repetition = repetition + 1.0
                
                if((occurrence/len(dataset))>=sup and (repetition/occurrence) >= conf):
                    
                    write_result(dataset[i], A_comb, B_comb, conf, sup)
                already_seen.append(my_hash)










